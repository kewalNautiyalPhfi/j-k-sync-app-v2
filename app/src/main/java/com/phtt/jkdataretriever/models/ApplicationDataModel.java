package com.phtt.jkdataretriever.models;

import android.content.Context;

/**
 * Created by Kewal on 06-10-2015.
 */
public class ApplicationDataModel {

    private ApplicationDataModel(){}

    private static class SingletonHolder {
        private static final ApplicationDataModel mInstance = new ApplicationDataModel();
    }

    public static ApplicationDataModel getInstance() {
        return SingletonHolder.mInstance;
    }


    private volatile String patientLid="";
    private volatile String patientGid="";
    private volatile Context activityContext=null;
    private volatile String patientName="";
    private volatile String patientAge="";
    private volatile String patientSex="";
    private volatile String patientMobile="";
    private volatile String patientPicName="";
    private volatile String height="";
    private volatile String weight="";
    private volatile String bmi="";
    private volatile String hwid="";

    public String getHwid() {
        return hwid;
    }

    public void setHwid(String hwid) {
        this.hwid = hwid;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }
    //    private volatile ClinicData clinicData=null;

//    public ClinicData getClinicData() {
//        return clinicData;
//    }
//
//    public void setClinicData(ClinicData clinicData) {
//        this.clinicData = clinicData;
//    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(String patientAge) {
        this.patientAge = patientAge;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }

    public String getPatientMobile() {
        return patientMobile;
    }

    public void setPatientMobile(String patientMobile) {
        this.patientMobile = patientMobile;
    }

    public String getPatientPicName() {
        return patientPicName;
    }

    public void setPatientPicName(String patientPicName) {
        this.patientPicName = patientPicName;
    }

    public Context getActivityContext() {
        return activityContext;
    }

    public void setActivityContext(Context activityContext) {
        this.activityContext = activityContext;
    }

    public String getPatientLid() {
        return patientLid;
    }

    public void setPatientLid(String patientLid) {
        this.patientLid = patientLid;
    }

    public String getPatientGid() {
        return patientGid;
    }

    public void setPatientGid(String patientGid) {
        this.patientGid = patientGid;
    }


    public void clearAllData(){
        setPatientLid(null);
        setPatientGid(null);
        setPatientPicName(null);
        setPatientAge(null);
        setPatientMobile(null);
        setPatientName(null);

    }
}
