package com.phtt.jkdataretriever.models;

/**
 * Created by Kewal on 05-10-2015.
 */
public class LoginData {

    private String status="";
    private String message="";
    private String patientLid="";
    private String patientGid="";
    private String userName="";


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPatientLid() {
        return patientLid;
    }

    public void setPatientLid(String patientLid) {
        this.patientLid = patientLid;
    }

    public String getPatientGid() {
        return patientGid;
    }

    public void setPatientGid(String patientGid) {
        this.patientGid = patientGid;
    }
}
