package com.phtt.jkdataretriever.parser;

import android.content.Context;

import com.phtt.jkdataretriever.database.ANCDataBase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by HP on 1/25/2016.
 */
public class ParseJsonObjParse {
    String json;
    HashMap<String,JSONArray> AllVals;
    ANCDataBase ancDb;//DB where data is inserted
    String jsonTitle;

    public ParseJsonObjParse(String input, Context mContext, String jsonTitle) {
        this.json=input;
//        ancDb=new ANCDataBase(mContext);
        this.jsonTitle=jsonTitle;
    }
    public void parseJsonObj(){

      /* gson method
       JsonElement jele =new JsonParser().parse(json);
        JsonObject jobj1= jele.getAsJsonObject();
        jobj1=jobj1.getAsJsonObject("ANC");
        JsonArray jarray = jobj1.getAsJsonArray("anc_advice");
        jobj1 = jarray.get(0).getAsJsonObject();
        String[] result = new String[0];
        result[0]= jobj1.get("id").toString();*/

//        JSONArray motherJsonObj=null;
        JSONObject motherJsonObj=null;
        try {
            motherJsonObj= new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject jsrootobj1 = motherJsonObj.optJSONObject(jsonTitle);

//        JSONObject jsrootobj2 = jsrootobj1.optJSONObject(jsonTitle);

        AllVals= getAllVals(jsrootobj1);

    }

    private HashMap<String,JSONArray> getAllVals (JSONObject jsonObject) {
        HashMap<String,JSONArray> keyValPair = null;

        Iterator a = jsonObject.keys();
        while(a.hasNext()) {
            String key = (String)a.next();
            // loop to get the dynamic key
            JSONArray value = null;
            try {
                value = (JSONArray)jsonObject.get(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            keyValPair.put(key,value);
        }
        return  keyValPair;
    }
}
