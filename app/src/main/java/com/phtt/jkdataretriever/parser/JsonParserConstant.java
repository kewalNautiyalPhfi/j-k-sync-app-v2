package com.phtt.jkdataretriever.parser;

/**
 * Created by HP on 1/21/2016.
 */
public class JsonParserConstant {
    public static final String PARSER_KEY_HEADER                                = "response_data";
    public static final String PARSER_KEY_STATUS                                = "status";
    public static final String PARSER_KEY_MESSAGE                               = "message";
    public static final String PARSER_KEY_PATIENT_LID                           = "patient_lid";
    public static final String PARSER_KEY_PATIENT_GID                           = "patient_gid";
    public static final String PARSER_KEY_UPLOAD_STATUS                         = "up_status";

}
