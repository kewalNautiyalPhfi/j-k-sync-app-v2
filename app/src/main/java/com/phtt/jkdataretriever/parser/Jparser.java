package com.phtt.jkdataretriever.parser;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.phtt.jkdataretriever.database.ANCDataBase;
import com.phtt.jkdataretriever.database.ChildCareDataBase;
import com.phtt.jkdataretriever.database.INCDataBase;
import com.phtt.jkdataretriever.database.PncDatabase;
import com.phtt.jkdataretriever.database.RegDataBase;

import com.phtt.jkdataretriever.database.swasthyaSlate;
import com.phtt.jkdataretriever.models.LoginData;
import com.phtt.jkdataretriever.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by HP on 1/21/2016.
 */
public class Jparser {
    private JSONObject jsonObject = null;
    private ContentValues values;

    public static final String geolocation="geolocation";
    public static final String MedFacilities="MedFacilities";
    public static final String patientinfo="patientinfo";
    public static final String regMapping="regMapping";
    public static final String relation="relation";
    public static final String whrabout="whrabout";
    RegDataBase RegDb;
    ANCDataBase ancDb;
    INCDataBase incDb;
    Context mContext;


    public LoginData parseLoginData(String jsonData) {
        LoginData model = null;

        try {
            jsonObject = new JSONObject(jsonData);
            if (null != jsonObject) {
                model = new LoginData();
                JSONObject loginDataObject = jsonObject.getJSONObject(JsonParserConstant.PARSER_KEY_HEADER);
                model.setStatus(loginDataObject.getString(JsonParserConstant.PARSER_KEY_STATUS));
                model.setMessage(loginDataObject.getString(JsonParserConstant.PARSER_KEY_MESSAGE));
                //model.setPatientLid(loginDataObject.getString(JsonParserConstant.PARSER_KEY_PATIENT_LID));
                // model.setPatientGid(loginDataObject.getString(JsonParserConstant.PARSER_KEY_PATIENT_GID));

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return model;

    }

    /* private void parseJsonArrayANC(JSONObject mJsonObject,Context con)
     {
         try {
             for(Iterator iterator =  mJsonObject.keys(); iterator.hasNext();) {
                 String tableName = (String) iterator.next();

                 //System.out.println(tableName+" -> "+mJsonObject.get(tableName));

                 if(!(mJsonObject.get(tableName).toString().equalsIgnoreCase("") ||mJsonObject.get(tableName)==null  )){
                     JSONArray mmJsonArray = mJsonObject.getJSONArray(tableName);
                     JSONObject jsonObject=null;
                     for (int i = 0; i < mmJsonArray.length(); i++) {
                         jsonObject = (JSONObject) mmJsonArray.getJSONObject(i);

                         for(Iterator iteratora =  jsonObject.keys(); iteratora.hasNext();) {
                             String key = (String) iteratora.next();
                             if (!key.equalsIgnoreCase("id")) {
                                 String valuesAnc=jsonObject.getString(key);
                                 System.out.println(i+key+" -> "+jsonObject.get(key));
                             }

                         }

                     }
                     ANCDataBase db=new ANCDataBase(con);
                     db.addDataAnc(jsonObject, tableName);
                     //Log.v(TAG,mmJsonArray.toString());
                 }
                 //Log.v(TAG,mmJsonArray.toString());
                     *//*

					for (int i = 0; i < mmJsonArray.length(); i++) {
						  JSONObject jsonObject = (JSONObject) mmJsonArray.getJSONObject(i);

						  for(Iterator iteratora =  jsonObject.keys(); iteratora.hasNext();) {
							    String key = (String) iteratora.next();
							    System.out.println(i+key+" -> "+jsonObject.get(key));
							}
					  }
					  *//*

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }*/
    public void parseInsertANCData(String response, Context HomeContext) throws JSONException {

        jsonObject = new JSONObject(response);
            if (null != jsonObject) {
                JSONObject responseObject = jsonObject.getJSONObject("ANC");
                if (responseObject != null) {

                    Iterator a = responseObject.keys();
                    String key;
                    String tabName = null;
                    while (a.hasNext()) {
                        key = (String) a.next();
                        // loop to get the dynamic key
                       JSONArray jsonArray = null;

                        switch (key) {
                            case "anc_ancvisitdetails":
                                ancDb = new ANCDataBase(HomeContext,"anc_ancvisitdetails");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject=null;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);//get elements(row) of the array(table)
                                            ContentValues values = values(myObject);

                                            insertAncRow(values);//insert row with ta


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_followup":
                                ancDb = new ANCDataBase(HomeContext,"anc_followup");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);

                                            insertAncRow(values);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }

                                break;
                            case "anc_nutrition":
                                ancDb = new ANCDataBase(HomeContext,"anc_nutrition");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_substance":
                                ancDb = new ANCDataBase(HomeContext,"anc_substance");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_gac":
                                ancDb = new ANCDataBase(HomeContext,"anc_gac");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_generalhealth":
                                ancDb = new ANCDataBase(HomeContext,"anc_generalhealth");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_gynecologyhistory":
                                ancDb = new ANCDataBase(HomeContext,"anc_gynecologyhistory");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }


                                break;
                            case "anc_laborsign":
                                ancDb = new ANCDataBase(HomeContext,"anc_laborsign");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_prevobstetrichistory":
                                ancDb = new ANCDataBase(HomeContext,"anc_prevobstetrichistory");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_psychosocial":
                                ancDb = new ANCDataBase(HomeContext,"anc_psychosocial");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_referral":
                                ancDb = new ANCDataBase(HomeContext,"anc_referral");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_symptomatic":
                                ancDb = new ANCDataBase(HomeContext,"anc_symptomatic");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_maternaldeath":
                                ancDb = new ANCDataBase(HomeContext,"anc_maternaldeath");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_picture":
                                ancDb = new ANCDataBase(HomeContext,"anc_picture");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_gps":
                                ancDb = new ANCDataBase(HomeContext,"anc_gps");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }

                                break;
                            case "anc_advice":
                                ancDb = new ANCDataBase(HomeContext,"anc_advice");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_finances":
                                ancDb = new ANCDataBase(HomeContext,"anc_finances");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_ancmothersdetails":
                                ancDb = new ANCDataBase(HomeContext,"anc_ancmothersdetails");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_basichealth":
                                ancDb = new ANCDataBase(HomeContext,"anc_basichealth");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues  values = values(myObject);

                                            insertAncRow(values);


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_birthplan":
                                ancDb = new ANCDataBase(HomeContext,"anc_birthplan");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);

                                            insertAncRow(values);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_contraception":
                                ancDb = new ANCDataBase(HomeContext,"anc_contraception");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);


                                            insertAncRow(values);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case "anc_fetal":
                                ancDb = new ANCDataBase(HomeContext,"anc_fetal");
                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }

                                break;
                            case "anc_familyhistory":
                                ancDb = new ANCDataBase(HomeContext,"anc_familyhistory");

                                jsonArray = responseObject.getJSONArray(key);
                                if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            ContentValues values = values(myObject);
                                            insertAncRow(values);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }

                            default:
                                break;
                        }

                    }

                }

            }
        }

    private synchronized void insertAncRow(ContentValues values) {
       // Log.d("values", "" + String.valueOf(values));
        if(values!=null)
            if(values.containsKey("id")){
                values.remove("id");
            }
            ancDb.addDataAnc(values);
    }

    private void insertIncRow(ContentValues values) {
        Log.d("values", "" + String.valueOf(values));
        if(values!=null)
            incDb.addDataInc(values);
    }

//    public void parseANC(String response, Context context) {
//        try {
//            jsonObject = new JSONObject(response);
//            if (null != jsonObject) {
//                JSONObject responseObject = jsonObject.getJSONObject("ANC");
//                if (responseObject != null) {
//                    JSONArray[] jsonArray = {responseObject.getJSONArray("anc_ancvisitdetails"),
//                            responseObject.getJSONArray("anc_followup"), responseObject.getJSONArray("anc_nutrition"),
//                            responseObject.getJSONArray("anc_substance"), responseObject.getJSONArray("anc_ancmothersdetails"),
//                            responseObject.getJSONArray("anc_basichealth"), responseObject.getJSONArray("anc_birthplan"),
//                            responseObject.getJSONArray("anc_contraception"), responseObject.getJSONArray("anc_familyhistory"),
//                            responseObject.getJSONArray("anc_fetal"), responseObject.getJSONArray("anc_finances"),
//                                responseObject.getJSONArray("anc_gac"), responseObject.getJSONArray("anc_generalhealth"),
//                            responseObject.getJSONArray("anc_gynecologyhistory"), responseObject.getJSONArray("anc_laborsign"),
//
//                            responseObject.getJSONArray("anc_prevobstetrichistory"), responseObject.getJSONArray("anc_psychosocial"),
//                            responseObject.getJSONArray("anc_referral"), responseObject.getJSONArray("anc_symptomatic"),
//                            responseObject.getJSONArray("anc_maternaldeath"), responseObject.getJSONArray("anc_picture"),
//                            responseObject.getJSONArray("anc_gps"), responseObject.getJSONArray("anc_advice")
//                    };
//                    processArray(jsonArray, context);
//                }
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//    }


//    public void processArray(JSONArray[] jsonArray, Context mContext) {
//        for (int i = 0; i < jsonArray.length; i++) {
//            if (jsonArray[i] != null && jsonArray[i].length() > 0) {
//                for (int j = 0; j < jsonArray[i].length(); j++) {
//                    JSONObject myObject;
//                    try {
//                        myObject = jsonArray[i].getJSONObject(j);
//                        values = values(myObject);
//                        values.remove("id");
//                        Log.d("values", "" + String.valueOf(values));
//
//                        ANCDataBase ancDb = new ANCDataBase(mContext);
//                        ancDb.addDataAnc(values, Constants.DOWNLOAD_TABLES_ANC[i]);
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//
//                }
//            }
//        }
//
//    }


    public ContentValues values(JSONObject jsonObj) {
        ContentValues myVals = new ContentValues();
        Iterator<String> keysIterator = jsonObj.keys();
        while (keysIterator.hasNext()) {
            String key = keysIterator.next();

            try {
                String value = String.valueOf(jsonObj.get(key));
                if(key.equalsIgnoreCase("id")){
                    continue;
                }else{
                    myVals.put(key, value);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return myVals;
    }


//    public void parseINC(String response, Context mContext) {
//        try {
//            jsonObject = new JSONObject(response);
//            if (null != jsonObject) {
//                JSONObject responseObject = jsonObject.getJSONObject("INC");
//                if (responseObject != null) {
//                    JSONArray[] jsonArray = {responseObject.getJSONArray("inc_status"),
//                            responseObject.getJSONArray("inc_newbornimmunization"), responseObject.getJSONArray("inc_complication"),
//                            responseObject.getJSONArray("inc_doctor"), responseObject.getJSONArray("inc_follow"),
//                            responseObject.getJSONArray("inc_atbirth"), responseObject.getJSONArray("inc_delivery"),
//                            responseObject.getJSONArray("inc_newborninfo"), responseObject.getJSONArray("inc_picture"),
//                            responseObject.getJSONArray("inc_breastfeeding"), responseObject.getJSONArray("inc_newborndefects"),
//                            responseObject.getJSONArray("inc_comphistory"), responseObject.getJSONArray("inc_gps"),
//                            responseObject.getJSONArray("inc_referral")
//                    };
////                    processArray(jsonArray, mContext);
//                }
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public void parseInsertRegData(String response, Context mContext) {

        try {
            jsonObject = new JSONObject(response);
            if (null != jsonObject) {
                JSONObject responseObject = jsonObject.getJSONObject("Registeration");
                if (responseObject != null) {
                /*    JSONArray[] jsonArray = {
//                            TODO: If following data is added, add in DOWNLOAD_TABLES_REG in Constants
//                            responseObject.getJSONArray("Agreement"), responseObject.getJSONArray("idproof"), responseObject.getJSONArray("key"),
//                            responseObject.getJSONArray("language"),
                            responseObject.getJSONArray("geolocation"),
                            responseObject.getJSONArray("MedFacilities"), responseObject.getJSONArray("patientinfo"),
                            responseObject.getJSONArray("regMapping"), responseObject.getJSONArray("relation"),
                            responseObject.getJSONArray("whereabout")
                    };
                    */
                    //            "geolocation","MedFacilities","patientinfo","regMapping","relation","whrabout"};//Local db table names matching the json names, except "Agreement","idproof","key","language"

                    Iterator a = responseObject.keys();
                    String key;
                    String tabName = null;
                    while(a.hasNext()) {
                        key = (String) a.next();
                        // loop to get the dynamic key
                        JSONArray jsonArray=null;

                        switch (key){
                            case geolocation:
                                jsonArray= responseObject.getJSONArray(key);
                                RegDb = new RegDataBase(mContext,key);
                                if(jsonArray.length()!=0 && jsonArray != null && jsonArray.length() > 0)
                                        for (int j = 0; j < jsonArray.length(); j++) {

                                            JSONObject myObject;
                                            try {
                                                myObject = jsonArray.getJSONObject(j);//get elements(row) of the array(table)
                                                values = values(myObject);
                                                if (values.containsKey("id")) {
                                                    values.remove("id");
                                                }
                                                if (values.containsKey("tab_datetime")) {
                                                    values.put("datetime", values.getAsString("tab_datetime"));
                                                    values.remove("tab_datetime");
                                                }
                                                if (values.containsKey("server_datetime")) {
                                                    values.remove("server_datetime");
                                                }
                                                if (values.containsKey("districtvhnd")) {
                                                    values.remove("districtvhnd");
                                                } if (values.containsKey("district_id")) {
                                                    values.remove("district_id");
                                                }
                                                if (values.containsKey("origin")) {
                                                    values.remove("origin");
                                                }if (values.containsKey("facility")) {
                                                    values.remove("facility");
                                                }if (values.containsKey("blockvhnd")) {
                                                    values.remove("blockvhnd");
                                                }if (values.containsKey("villagevhnd")) {
                                                    values.remove("villagevhnd");
                                                }if (values.containsKey("block")) {
                                                    values.remove("block");
                                                }if (values.containsKey("homephone")) {
                                                    values.remove("homephone");
                                                }if (values.containsKey("block_id")) {
                                                    values.remove("block_id");
                                                }

                                                Log.d("Regvalues", "" + String.valueOf(values));

                                                insertRegRow(values);//insert row with ta


                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        }


                                break;
                            case MedFacilities:
                                tabName="medical";
                                jsonArray= responseObject.getJSONArray(key);
                                RegDb = new RegDataBase(mContext,tabName);
                                if(jsonArray.length()!=0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            values = values(myObject);
                                            if (values.containsKey("id")) {
                                                values.remove("id");
                                            }
                                            if (values.containsKey("tab_datetime")) {
                                                values.put("datetime", values.getAsString("tab_datetime"));
                                                values.remove("tab_datetime");
                                            }
                                            if (values.containsKey("server_datetime")) {
                                                values.remove("server_datetime");
                                            }
                                            if (values.containsKey("docno")) {
                                                values.put("doctorno", values.getAsString("docno"));
                                                values.remove("docno");
                                            } if (values.containsKey("doc")) {
                                                values.put("physician", values.getAsString("doc"));
                                                values.remove("doc");
                                            }
                                            Log.d("Regvalues", "" + String.valueOf(values));

                                            insertRegRow(values);//insert row with ta


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }

                                break;
                            case patientinfo:
                                jsonArray= responseObject.getJSONArray(key);
                                RegDb = new RegDataBase(mContext,key);
                                if(jsonArray.length()!=0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            values = values(myObject);
                                            if (values.containsKey("id")) {
                                                values.remove("id");
                                            }
                                            if (values.containsKey("pid")) {
                                                values.remove("pid");
                                            }

//                                            if (values.containsKey("gid")) {
//                                                values.put("pid",values.getAsString(""));
//                                                values.remove("gid");
//                                            }
                                            if (values.containsKey("hwid")) {
                                                values.remove("hwid");
                                            }
                                            if (values.containsKey("tab_datetime")) {
                                                values.put("datetime", values.getAsString("tab_datetime"));
                                                values.remove("tab_datetime");
                                            }
                                            if (values.containsKey("server_datetime")) {
                                                values.remove("server_datetime");
                                            }
                                            if (values.containsKey("identity_no")) {
                                                values.put("identityNo", values.getAsString("identity_no"));
                                                values.remove("identity_no");
                                            }
                                            if (values.containsKey("bank_pic")) {
                                                values.put("bankPic", values.getAsString("bank_pic"));
                                                values.remove("bank_pic");
                                            }
                                            if (values.containsKey("identity_pic")) {
                                                values.put("identityPic", values.getAsString("identity_pic"));
                                                values.remove("identity_pic");
                                            }
                                            if (values.containsKey("bank_no")) {
                                                values.put("bankno", values.getAsString("bank_no"));
                                                values.remove("bank_no");
                                            }
                                            if (values.containsKey("bloodgroup")) {
                                                values.remove("bloodgroup");
                                            }if (values.containsKey("diagnostic_status")) {
                                                values.remove("diagnostic_status");
                                            }if (values.containsKey("hwid")) {
                                                values.remove("hwid");
                                            }
                                            Log.d("Regvalues", "" + String.valueOf(values));

                                            insertRegRow(values);//insert row with ta


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case regMapping:
                                RegDb = new RegDataBase(mContext,key);
                                jsonArray= responseObject.getJSONArray(key);
                                if(jsonArray.length()!=0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            values = values(myObject);
////                                            if (values.containsKey("id")) {
////                                                values.remove("id");
////                                            }
////
////                                            if (values.containsKey("tab_datetime")) {
//////                                                values.put("datetime", values.getAsString("tab_datetime"));
////                                                values.remove("tab_datetime");
////                                            }
////                                            if (values.containsKey("server_datetime")) {
////                                                values.remove("server_datetime");
////                                            }
////
////                                            if (values.containsKey("picture")) {
////                                                values.remove("picture");
////                                            }if (values.containsKey("longitude")) {
////                                                values.remove("longitude");
////                                            }if (values.containsKey("latitude")) {
////                                                values.remove("latitude");
////                                            }if (values.containsKey("pid")) {
////                                                values.put("lid",values.getAsString("pid"));
////                                                values.remove("pid");
//
//                                            }
                                            Log.d("Regvalues", "" + String.valueOf(values));

                                            insertRegRow(values);


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                break;
                            case relation:
                                jsonArray= responseObject.getJSONArray(key);
                                RegDb = new RegDataBase(mContext,key);
                                if(jsonArray.length()!=0 && jsonArray != null && jsonArray.length() > 0){
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                                JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            values = values(myObject);
                                            if (values.containsKey("id")) {
                                                values.remove("id");
                                            }
                                            if (values.containsKey("contact")) {
                                                values.put("contactum", values.getAsString("contact"));
                                                values.remove("contact");
                                            }
                                            if (values.containsKey("emergency1")) {
                                                values.put("emergcontact1", values.getAsString("emergency1"));
                                                values.remove("emergency1");
                                            }
                                            if (values.containsKey("emergency2")) {
                                                values.put("emergcontact2", values.getAsString("emergency2"));
                                                values.remove("emergency2");
                                            }
                                            if (values.containsKey("emergency2")) {
                                                values.put("emergcontact2", values.getAsString("emergency2"));
                                                values.remove("emergency2");
                                            }
                                            if (values.containsKey("relationtype")) {
                                                values.remove("relationtype");
                                            }
                                            Log.d("Regvalues", "" + String.valueOf(values));

                                            insertRegRow(values);//insert row with ta
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    }


                                break;

                            case whrabout:
                                tabName="whereabout";
                                RegDb = new RegDataBase(mContext,tabName);
                                jsonArray= responseObject.getJSONArray(key);
                                if(jsonArray.length()!=0 && jsonArray != null && jsonArray.length() > 0)
                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject myObject;
                                        try {
                                            myObject = jsonArray.getJSONObject(j);
                                            values = values(myObject);
                                            if (values.containsKey("id")) {
                                                values.remove("id");
                                            }
//                                            if (values.containsKey("gid")) {
//                                                values.remove("gid");
//                                            }
                                            if (values.containsKey("tab_datetime")) {
                                                values.put("datetime", values.getAsString("tab_datetime"));
                                                values.remove("tab_datetime");
                                            }
                                            if (values.containsKey("server_datetime")) {
                                                values.remove("server_datetime");
                                            }
                                            Log.d("Regvalues", "" + String.valueOf(values));

                                            insertRegRow(values);


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }

                                break;
                            default:
                                break;
                        }

                    }

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    private void insertRegRow(ContentValues values) {
        RegDb.addRegData(values);
    }


    public void parsePnc(String response, Context context) {
        try {
            jsonObject = new JSONObject(response);
            if (null != jsonObject) {
                JSONObject responseObject = jsonObject.getJSONObject("PNC");

                if (responseObject != null) {



                    Iterator<String> keys= responseObject.keys();
//                    Iterator<String> keysT = keys;
//                    while (keysT.hasNext()){
//                        String tableName = keysT.next();
//                        SQLiteDatabase database = SQLiteDatabase.openDatabase(Constants.PNC_APP_DATABASE_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);
//
//                        if(database!=null && tableName!=null && !tableName.isEmpty())
//                            database.delete(tableName, null, null);
//                        database.close();
//
//                    }
                    while (keys.hasNext()){

                        String tableName = keys.next();
                        JSONArray dataArray = responseObject.getJSONArray(tableName);

                        processPncdArray(dataArray, context, tableName);
                    }

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void processPncdArray(JSONArray data, Context mContext, String tableName) {

        if(null!=data && data.length()>0){


            for(int i=0;i<data.length();i++){

                try {
                    JSONObject object = data.getJSONObject(i);
                    ContentValues values = values(object);


                    PncDatabase pncDb = new PncDatabase(mContext,tableName);
                    pncDb.addDataPNC(values, tableName);


                } catch (Exception e) {
                    e.printStackTrace();


                }
            }

        }


    }



    public void parseChildCare(String response, Context context) {
        try {
            jsonObject = new JSONObject(response);
            if (null != jsonObject) {
                JSONObject responseObject = jsonObject.getJSONObject("ChildCare");

                if (responseObject != null) {

                    Iterator<String> keys= responseObject.keys();
//                    Iterator<String> keysT =keys;
//                    while (keysT.hasNext()){
//                        String tableName = keysT.next();
//                        SQLiteDatabase childDB = SQLiteDatabase.openDatabase(Constants.CHILD_CARE_APP_DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
//
//
//                        if(childDB!=null && tableName!=null && !tableName.isEmpty())
//                            childDB.delete(tableName, null, null);
//                        childDB.close();
//                    }
                    while (keys.hasNext()){

                        String tableName = keys.next();
                        JSONArray dataArray = responseObject.getJSONArray(tableName);

                        processChildArray(dataArray, context, tableName);
                    }

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void processChildArray(JSONArray data, Context mContext, String tableName) {

        if(null!=data && data.length()>0){



            for(int i=0;i<data.length();i++){

                try {
                    JSONObject object = data.getJSONObject(i);
                    ContentValues values = values(object);


                    ChildCareDataBase childDb = new ChildCareDataBase(mContext,tableName);
                    childDb.addDataChildCare(values, tableName);


                } catch (Exception e) {
                    e.printStackTrace();


                }
            }

        }


    }



    public void parseSwasthyaSlate(String response, Context context) {
        try {
            jsonObject = new JSONObject(response);
            if (null != jsonObject) {
                JSONObject responseObject = jsonObject.getJSONObject("Diagnostic");

                if (responseObject != null) {




                  Iterator<String> keys= responseObject.keys();
//                    Iterator<String> keysT = keys;
//                    while (keysT.hasNext()){
//                        String tableName = keysT.next();
//                        SQLiteDatabase SsdDB = SQLiteDatabase.openDatabase(Constants.SWASTHYA_SLATE_APP_DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
//
//                        if(SsdDB!=null && tableName!=null && !tableName.isEmpty())
//                            SsdDB.delete(tableName, null, null);
//                        SsdDB.close();
//                    }

                    while (keys.hasNext()){

                        String tableName = keys.next();
                        JSONArray dataArray = responseObject.getJSONArray(tableName);

                        processSwasthyaSlateArray(dataArray, context, tableName);
                    }

//                    JSONArray[] jsonArray = {
//
//
//                            responseObject.getJSONArray("weight"),
//                            responseObject.getJSONArray("pulse_oximeter"),
//                            responseObject.getJSONArray("pregnancy_hcg"),
//                            responseObject.getJSONArray("temperature"),
//                            responseObject.getJSONArray("urine_protein"),
//                            responseObject.getJSONArray("hiv"),
//                            responseObject.getJSONArray("blood_glucose"),
//                            responseObject.getJSONArray("malaria"),
//                            responseObject.getJSONArray("haemoglobin"),
//                            responseObject.getJSONArray("urine_sugar"),
//                            responseObject.getJSONArray("blood_pressure"),
//                            responseObject.getJSONArray("cholesterol_test"),
//                            responseObject.getJSONArray("pt_inr_test"),
//                            responseObject.getJSONArray("foetal_doppler"),
//                            responseObject.getJSONArray("syphillis")
//                    };


                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void processSwasthyaSlateArray(JSONArray data, Context mContext, String tableName) {

        if(null!=data && data.length()>0){



            for(int i=0;i<data.length();i++){

                try {
                    JSONObject object = data.getJSONObject(i);
                    ContentValues values = values(object);
//

                    swasthyaSlate SsDb = new swasthyaSlate(mContext,tableName);
                    SsDb.addDataSwasthyaSlate(values,tableName);


                } catch (Exception e) {
                    e.printStackTrace();


                }
            }

        }


    }


    public ArrayList<String> parseRegistrationImagePath(String response, Context mContext) {
            if(response!=null ){
                ArrayList<String> image_path=null;
                try {
                    JSONObject json_obj=new JSONObject(response);
                    if(json_obj!=null && json_obj.length()>0){
                        JSONArray json_arr=json_obj.optJSONArray("Registeration");
                        if(json_arr!=null && json_arr.length()>0){
                           image_path=new ArrayList<>();
                            for(int i=0;i<json_arr.length();i++){
                                JSONObject path_json=json_arr.optJSONObject(i);
                                image_path.add(path_json.optString("image_path"));
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return image_path;
            }

        return null;
    }

    public ArrayList<String> parsePNCImagePath(String response, Context mContext) {
        if(response!=null ){
            ArrayList<String> image_path=null;
            try {
                JSONObject json_obj=new JSONObject(response);
                if(json_obj!=null && json_obj.length()>0){
                    JSONArray json_arr=json_obj.optJSONArray("PNC_IMAGES");
                    if(json_arr!=null && json_arr.length()>0){
                        image_path=new ArrayList<>();
                        for(int i=0;i<json_arr.length();i++){
                            JSONObject path_json=json_arr.optJSONObject(i);
                            image_path.add(path_json.optString("image_path"));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return image_path;
        }

        return null;
    }




    public ArrayList<String> parseChildCareImagePath(String response, Context mContext) {
        if(response!=null ){
            ArrayList<String> image_path=null;
            try {
                JSONObject json_obj=new JSONObject(response);
                if(json_obj!=null && json_obj.length()>0){
                    JSONArray json_arr=json_obj.optJSONArray("CHILD_IMAGES");
                    if(json_arr!=null && json_arr.length()>0){
                        image_path=new ArrayList<>();
                        for(int i=0;i<json_arr.length();i++){
                            JSONObject path_json=json_arr.optJSONObject(i);
                            image_path.add(path_json.optString("image_path"));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return image_path;
        }

        return null;
    }


    public ArrayList<String> parseANCImagePath(String response, Context mContext) {
        if(response!=null ){
            ArrayList<String> image_path=null;
            try {
                JSONObject json_obj=new JSONObject(response);
                if(json_obj!=null && json_obj.length()>0){
                    JSONArray json_arr=json_obj.optJSONArray("ANC");
                    if(json_arr!=null && json_arr.length()>0){
                        image_path=new ArrayList<>();
                        for(int i=0;i<json_arr.length();i++){
                            JSONObject path_json=json_arr.optJSONObject(i);
                            image_path.add(path_json.optString("image_path"));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return image_path;
        }

        return null;
    }


    public ArrayList<String> parseINCImagePath(String response, Context mContext) {
        if(response!=null ){
            ArrayList<String> image_path=null;
            try {
                JSONObject json_obj=new JSONObject(response);
                if(json_obj!=null && json_obj.length()>0){
                    JSONArray json_arr=json_obj.optJSONArray("INC");
                    if(json_arr!=null && json_arr.length()>0){
                        image_path=new ArrayList<>();
                        for(int i=0;i<json_arr.length();i++){
                            JSONObject path_json=json_arr.optJSONObject(i);
                            image_path.add(path_json.optString("image_path"));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return image_path;
        }

        return null;
    }




    public void parseInsertINCData(String response, Context HomeContext) throws JSONException {


        jsonObject = new JSONObject(response);
        if (null != jsonObject) {
            JSONObject responseObject = jsonObject.getJSONObject("INC");
            if (responseObject != null) {

                Iterator a = responseObject.keys();
                String key;
                String tabName = null;
                while (a.hasNext()) {
                    key = (String) a.next();
                    // loop to get the dynamic key
                    JSONArray jsonArray = null;

                    switch (key) {
                        case "inc_status":
                            incDb = new INCDataBase(HomeContext,"inc_status");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);//get elements(row) of the array(table)
                                        values = values(myObject);
                                        insertIncRow(values);//insert row with ta


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_newbornimmunization":
                            incDb = new INCDataBase(HomeContext,"inc_newbornimmunization");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }

                            break;
                        case "inc_complication":
                            incDb = new INCDataBase(HomeContext,"inc_complication");

                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_doctor":
                            incDb = new INCDataBase(HomeContext,"inc_doctor");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);

                                        insertIncRow(values);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_follow":
                            incDb = new INCDataBase(HomeContext,"inc_follow");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_atbirth":
                            incDb = new INCDataBase(HomeContext,"inc_atbirth");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_delivery":
                            incDb = new INCDataBase(HomeContext,"inc_delivery");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }


                            break;
                        case "inc_newborninfo":
                            incDb = new INCDataBase(HomeContext,"inc_newborninfo");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_picture":
                            incDb = new INCDataBase(HomeContext,"inc_picture");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_breastfeeding":
                            incDb = new INCDataBase(HomeContext,"inc_breastfeeding");

                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_newborndefects":
                            incDb = new INCDataBase(HomeContext,"inc_newborndefects");

                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_comphistory":
                            incDb = new INCDataBase(HomeContext,"inc_comphistory");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_gps":
                            incDb = new INCDataBase(HomeContext,"inc_gps");

                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;
                        case "inc_referral":
                            incDb = new INCDataBase(HomeContext,"inc_referral");
                            jsonArray = responseObject.getJSONArray(key);
                            if (jsonArray.length() != 0 && jsonArray != null && jsonArray.length() > 0)
                                for (int j = 0; j < jsonArray.length(); j++) {

                                    JSONObject myObject;
                                    try {
                                        myObject = jsonArray.getJSONObject(j);
                                        values = values(myObject);
                                        insertIncRow(values);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            break;

                        default:
                            break;
                    }

                }

            }

        }
    }
}