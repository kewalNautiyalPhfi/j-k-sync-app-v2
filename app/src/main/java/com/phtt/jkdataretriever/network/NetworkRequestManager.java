package com.phtt.jkdataretriever.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phtt.jkdataretriever.utils.Constants;
import com.phtt.jkdataretriever.utils.INetResult;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class is used to generate service request params at runtime
 *
 * Created by Kewal on 06-10-2015.
 */

public class NetworkRequestManager {

    Context mContext;
    RequestQueue requestQueue;
    String url=null;
    volatile ProgressDialog pDialog=null;
    INetResult listener=null;

    public NetworkRequestManager(Context mContext){

        this.mContext = mContext;
        requestQueue=VolleySingleton.getInstance().getRequestQueue();

    }

    public NetworkRequestManager(Context mContext, INetResult listener){

        this.mContext = mContext;
        requestQueue=VolleySingleton.getInstance().getRequestQueue();
        this.listener = listener;
    }

    public void generateServiceRequestParams(int requestId, Bundle bundle){
        Map<String,JSONObject> params=new HashMap<String,JSONObject>();
        JSONObject jsonObject=null;
        String url = null;

        switch (requestId){
            case NetworkConstants.REQUEST_ID_LOGIN:
                 // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.LOGIN_API;

                break;
            case NetworkConstants.REQUEST_ID_DOWNLOAD_ANC:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.DOWNLOAD_API_ANC;

                break;
            case NetworkConstants.REQUEST_ID_DOWNLOAD_INC:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.DOWNLOAD_API_INC;

                break;
            case NetworkConstants.REQUEST_ID_DOWNLOAD_REG:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.DOWNLOAD_API_Reg;

                break;
            case NetworkConstants.REQUEST_ID_DOWNLOAD_PNC:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.DOWNLOAD_API_PNC;

                break;



            case NetworkConstants.REQUEST_ID_DOWNLOAD_CHILD_CARE:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.DOWNLOAD_API_CHILD_CARE;

                break;

            case NetworkConstants.REQUEST_ID_DOWNLOAD_SWASTHYA_SLATE:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.DOWNLOAD_API_SWASTHYA_SLATE;

                break;

            case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_REGISTRATION:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.IMAGE_DOWNLOAD_API_REGISTRATION;

                break;


            case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_PNC:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.IMAGE_DOWNLOAD_API_PNC;

                break;

            case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_CHILD_CARE:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.IMAGE_DOWNLOAD_API_CHILD_CARE;
                break;

            case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_ANC:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.IMAGE_DOWNLOAD_API_ANC;
                break;

            case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_INC:
                // this method generate json request and return Json object
                jsonObject = generateJSONObject(bundle);
                url= Constants.IMAGE_DOWNLOAD_API_INC;
                break;


//            case NetworkConstants.REQUEST_ID_FORGOT_PASSWORD:
//                jsonObject = generateJSONObject(bundle);
////                url=Constants.FORGET_PASSWORD_API;
//                break;

            default:
                break;
        }

        if(jsonObject!=null &&!jsonObject.toString().equals("{}")) {
            params.put("request_data", jsonObject);

            if (!params.isEmpty()) {
                // make service call and process the response
                makeServiceCall(this.mContext, requestId, Request.Method.POST, url, params);
            }
        }else if(jsonObject.toString().equals("{}")){
            Toast.makeText(mContext,"No Data To Upload",Toast.LENGTH_LONG).show();
        }

    }


    /**
     * this method is used to make service call
     * once request params is generated successfully
     * @param mContext
     * @param method
     * @param serviceUrl
     * @param params
     */
    private void makeServiceCall(final Context mContext, final int requestId, int method, String serviceUrl,  Map<String,JSONObject> params){

        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(method,serviceUrl, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
//                            Log.d("server_response", response.toString());
                            // send server response to NetworkResponseManager for further action
                            NetworkResponseManager nrm = new NetworkResponseManager();
                            nrm.processServerResponse(mContext, requestId, response, listener);

                        }catch (Exception e){
                            e.printStackTrace();
                        }finally {
                            try{
                                if(pDialog!=null){
                                pDialog.cancel();
                                pDialog.dismiss();}
                            }catch (Exception e){
                                e.printStackTrace();}
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            if(null!=error){
                                if(error.getClass().equals(TimeoutError.class)) {
                                   listener.onResult(false,"TimeOut Error.");
                                }else {
                                    listener.onResult(false, error.getMessage().toString());
                                    Log.d("volley_error", error.getMessage());
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();

                        }finally {
                            try{
                                pDialog.cancel();
                                pDialog.dismiss();
                            }catch (Exception e){e.printStackTrace();}

                        }
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", "My useragent");
                return headers;
            }
        };



        int socketTimeout = 50000;//50 sec onds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsObjRequest.setRetryPolicy(policy);
        requestQueue.add(jsObjRequest);
    }

    /**
     * this method generate json object
     * @param bundle
     * @return
     */
    private JSONObject generateJSONObject(Bundle bundle){

        JSONObject jsonObject =new JSONObject();
        Set<String> keyset = bundle.keySet();
        try {
            for (String key : keyset) {
                jsonObject.put(key, bundle.getString(key));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

}
