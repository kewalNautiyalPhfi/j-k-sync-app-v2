package com.phtt.jkdataretriever.network;

import android.util.Log;


import com.phtt.jkdataretriever.database.DbConstant;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;


public class ServiceHandler {
	public final static int GET = 1;
	public final static int POST = 2;
	public static final int TIME_OUT_CONNECTION=30*1000;
    
	public String makeServiceCall(String url, int method) {
		return this.makeServiceCall(url, method, null,null);
	}

	public String makeServiceCall(String url, int method,
			List<NameValuePair> params,String token) {
		String response = null;
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			// Checking http request method type
			if (method == POST) {
				HttpPost httpPost = new HttpPost(url);
				
				// adding post params
				if (params != null) {
					httpPost.setEntity(new UrlEncodedFormEntity(params));
				}

				if(token!=null){
					httpPost.setHeader("Authorization","Token "+token);
				}
				
				httpResponse = httpClient.execute(httpPost);

			} else if (method == GET) {
				// appending params to url
				if (params != null) {
					String paramString = URLEncodedUtils
							.format(params, "utf-8");
					url += "?" + paramString;
				}
				HttpGet httpGet = new HttpGet(url);
				
				if(token!=null){
					httpGet.setHeader("Authorization","Token "+token);
				}
				
				httpResponse = httpClient.execute(httpGet);

			}
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return response;

	}
	
	public String executeHttpPostImage(String url, byte[] data,
			JSONObject jsonObj,String image_name) throws Exception {

		BufferedReader in = null;
		StringBuffer sb = null;
		String result = "";
		// Log.e("size of data ", ""+data.length);
		try {
			// InetAddress address = InetAddress.getByName(url);
			HttpClient client = getHttpClient();
			 Log.e("JSON OBJECT", ""+jsonObj.toString());
			HttpPost request = new HttpPost(url);
			//request.setHeader("Authorization", "Token ");

			try {
				MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
				if (data != null) {
					ByteArrayBody toUpload = new ByteArrayBody(data,image_name);
					reqEntity.addPart("image", toUpload);
				}

//				reqEntity.addPart(DbConstant.COLUMN_LID, new StringBody(jsonObj.getString(DbConstant.COLUMN_LID)));
				// reqEntity.addPart("FileName", new StringBody("androidpic"));
				// reqEntity.addPart("data", new
				// StringBody(jsonObj.toString()));
				// reqEntity.addPart("data", new
				// StringBody(postParameters.toString()));
				request.setEntity(reqEntity);
				HttpResponse response = client.execute(request);
				
				in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				sb = new StringBuffer("");
				String line = "";
				String NL = System.getProperty("line.separator");
				while ((line = in.readLine()) != null) {
					sb.append(line + NL);
					Log.e("in hile", line);
				}
				in.close();
			} catch (Exception e) {
				Log.e("Exceptoion", e.getMessage());
			}
			result = sb.toString();
			return result;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
					Log.e("Exception", e.getMessage());
				}

			}

		}
	}

	

	
	
	public static HttpClient getHttpClient(){
		HttpClient mhttpclient=null;
		if(mhttpclient==null){
			mhttpclient=new DefaultHttpClient(); 
			final HttpParams mhttpparams=mhttpclient.getParams();
			HttpConnectionParams.setSoTimeout(mhttpparams, TIME_OUT_CONNECTION);
			HttpConnectionParams.setConnectionTimeout(mhttpparams, TIME_OUT_CONNECTION);
		}
		return mhttpclient;
	}
}
