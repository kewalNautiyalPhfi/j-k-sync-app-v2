package com.phtt.jkdataretriever.network;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import com.phtt.jkdataretriever.parser.Jparser;
import com.phtt.jkdataretriever.models.LoginData;
import com.phtt.jkdataretriever.utils.Constants;
import com.phtt.jkdataretriever.utils.INetResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by Kewal on 06-10-2015.
 */
public class NetworkResponseManager {
    Jparser parser;

    /**
     * process server response for json parsing
     *
     * @param mContext
     * @param requestId
     * @param response
     */
    public void processServerResponse(Context mContext, int requestId, JSONObject response, INetResult listener) throws JSONException {

        Intent intent = null;
        ArrayList<String> all_image_path = null;

        if (null != response) {

//            notify = new NotificationsDisplay(mContext);

            switch (requestId) {
                case NetworkConstants.REQUEST_ID_LOGIN:

                    parser = new Jparser();
                    LoginData model = parser.parseLoginData(response.toString());
                    if (null != model && model.getStatus().contains(NetworkConstants.MESSAGE_SUCCESS)) {
                        listener.onResult(true, model.getMessage(), model);
                    } else {
                        listener.onResult(false, model.getMessage(), model);
                    }

                    break;

                case NetworkConstants.REQUEST_ID_DOWNLOAD_ANC:

                    if (null != response.toString() && !response.toString().isEmpty()) {

                        parser = new Jparser();
                        parser.parseInsertANCData(response.toString(), mContext);

                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_ANC, true, "success");
                    } else {
                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_ANC, true, "failed");
                    }

                    break;
                case NetworkConstants.REQUEST_ID_DOWNLOAD_INC:

                    if (null != response.toString() && !response.toString().isEmpty()) {
                        parser = new Jparser();
                        parser.parseInsertINCData(response.toString(), mContext);
                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_INC, true, "success");
                    } else {
                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_INC, true, "failed");
                    }

                    break;

                case NetworkConstants.REQUEST_ID_DOWNLOAD_REG:

                    if (null != response.toString() && !response.toString().isEmpty()) {

                        parser = new Jparser();
                        parser.parseInsertRegData(response.toString(), mContext);
                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_REGISTRATION, true, "success");
                    } else {
                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_REGISTRATION, true, "failed");
                    }

                    break;

                case NetworkConstants.REQUEST_ID_DOWNLOAD_PNC:

                    if (null != response.toString() && !response.toString().isEmpty()) {

                        parser = new Jparser();
                        parser.parsePnc(response.toString(), mContext);

                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_PNC, true, "success");
                    } else {
                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_PNC, true, "failed");
                    }
                    break;

                case NetworkConstants.REQUEST_ID_DOWNLOAD_CHILD_CARE:

                    if (null != response.toString() && !response.toString().isEmpty()) {

                        parser = new Jparser();
                        parser.parseChildCare(response.toString(), mContext);

                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_CHILD_CARE, true, "success");
                    } else {
                        listener.onResult(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_CHILD_CARE, true, "failed");
                    }

                    break;


                case NetworkConstants.REQUEST_ID_DOWNLOAD_SWASTHYA_SLATE:

                    parser = new Jparser();
                    parser.parseSwasthyaSlate(response.toString(), mContext);

                    break;


                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_REGISTRATION:

                    parser = new Jparser();
                    all_image_path = parser.parseRegistrationImagePath(response.toString(), mContext);
                    if (all_image_path != null && all_image_path.size() > 0) {
                        listener.onResult(true, "RegPicture",all_image_path);
                    } else {
                        listener.onResult(false,"RegPicture",null);
                    }
                    break;


                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_PNC:

                    parser = new Jparser();
                    all_image_path = parser.parsePNCImagePath(response.toString(), mContext);
                    if (all_image_path != null && all_image_path.size() > 0) {
                        listener.onResult(true, "PNCpicture",all_image_path);
                    } else {
                        listener.onResult(false,"PNCpicture",null);
                    }
                    break;


                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_CHILD_CARE:

                    parser = new Jparser();
                    all_image_path = parser.parseChildCareImagePath(response.toString(), mContext);
                    if (all_image_path != null && all_image_path.size() > 0) {
                        listener.onResult(true, "ChildCarePic",all_image_path);
                    } else {
                        listener.onResult(false,"ChildCarePic",null);
                    }
                    break;

                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_ANC:

                    parser = new Jparser();
                    all_image_path = parser.parseANCImagePath(response.toString(), mContext);
                    if (all_image_path != null && all_image_path.size() > 0) {
                        listener.onResult(true, "ANCpicture",all_image_path);
                    } else {
                        listener.onResult(false,"ANCpicture",null);
                    }
                    break;


                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_INC:

                    parser = new Jparser();
                    all_image_path = parser.parseINCImagePath(response.toString(), mContext);
                    if (all_image_path != null && all_image_path.size() > 0) {
                        listener.onResult(true, "INCpicture",all_image_path);
                    } else {
                        listener.onResult(false,"INCpicture",null);
                    }
                    break;


//                case NetworkConstants.REQUEST_ID_FORGOT_PASSWORD:
//                  Jparser = new Jparser();
//                    ForgotPasswordData dataModel = Jparser.parserForgotPasswordData(response.toString());
//                    try {
//                        if (null != dataModel && dataModel.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
//                            listener.onResult(true, dataModel.getMessage());
//                        } else {
//                            listener.onResult(false, dataModel.getMessage());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    break;


                default:
                    break;

            }

        }

    }
}
