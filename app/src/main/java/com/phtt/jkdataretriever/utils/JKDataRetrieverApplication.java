package com.phtt.jkdataretriever.utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by HP on 1/21/2016.
 */
public class JKDataRetrieverApplication extends Application{
        public static JKDataRetrieverApplication appInstance;

        @SuppressWarnings("unused")
        @Override
        public void onCreate() {
//        if (Constants.Config.DEVELOPER_MODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
//            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDialog().build());
//            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build());
//        }

            super.onCreate();

            appInstance = this;
        }


        public static JKDataRetrieverApplication getAppInstance() {
            return appInstance;
        }

        public static Context getAppContext() {
            return appInstance.getApplicationContext();
        }
    }


