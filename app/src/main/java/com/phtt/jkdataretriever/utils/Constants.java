package com.phtt.jkdataretriever.utils;

/**
 * Created by HP on 1/21/2016.
 */
public class Constants {


//    public static final String LOGIN_API="http://swasthyaslate.org/webservicelogin/login.php";

    public static final String LOGIN_API="http://swasthyaslate.org/phfiadministration/webservice/hw_anc_download.php";
    public static final String DOWNLOAD_API_ANC="http://swasthyaslate.org/phfiadministration/webservice/hw_anc_download.php";
    public static final String DOWNLOAD_API_PNC="http://swasthyaslate.org/phfiadministration/webservice/hw_pnc_download.php";
    public static final String DOWNLOAD_API_INC="http://swasthyaslate.org/phfiadministration/webservice/hw_inc_download.php";
    public static final String DOWNLOAD_API_Reg="http://swasthyaslate.org/phfiadministration/webservice/hw_reg_download.php";
    public static final String DOWNLOAD_API_SWASTHYA_SLATE ="http://swasthyaslate.org/phfiadministration/webservice/hw_diagnostic_download.php";
    public static final String DOWNLOAD_API_CHILD_CARE ="http://swasthyaslate.org/phfiadministration/webservice/hw_childcare_download.php";


    public static final String IMAGE_DOWNLOAD_API_REGISTRATION   = "http://swasthyaslate.org/phfiadministration/webservice/hw_registration_download_pic.php";
    public static final String IMAGE_DOWNLOAD_API_PNC            = "http://www.swasthyaslate.org/phfiadministration/webservice/download_pnc_pictures.php";
    public static final String IMAGE_DOWNLOAD_API_CHILD_CARE     = "http://www.swasthyaslate.org/phfiadministration/webservice/download_child_pictures.php";
    public static final String IMAGE_DOWNLOAD_API_ANC            ="http://swasthyaslate.org/phfiadministration/webservice/hw_anc_download_picture.php";
    public static final String IMAGE_DOWNLOAD_API_INC            ="http://swasthyaslate.org/phfiadministration/webservice/hw_inc_download_pic.php";


    public static final String[] DOWNLOAD_TABLES_ANC = {"anc_ancvisitdetails","anc_followup","anc_nutrition","anc_substance", "anc_ancmothersdetails", "anc_basichealth", "anc_birthplan","anc_contraception","anc_familyhistory","anc_fetal","anc_finances","anc_gac","anc_generalhealth","anc_gynecologyhistory","anc_laborsign","anc_prevobstetrichistory","anc_psychosocial","anc_referral","anc_symptomatic", "anc_maternaldeath","anc_picture","anc_gps","anc_advice"};

    public static final String[] DOWNLOAD_TABLES_PNC = {"pncreferral","pnchistory","pncgpslocation","pncchildfollowup","mothercounsellingregardinginfantcare","mothercontraception","pncsummarynotes","infantdeath","motherbreastfeeding","pncregistration","infantphysicalexamination","pncadvice","pncchildreferral","mothercounsellingonbreastfeeding","motherdangersign","referralofmotherandinfant","pncpicture",
                                                        "motherdeath","motherpostpartumcarenhygeine","pncvisitdetails","infantcomplications","registrationofbirth","motherphysicalexamination","pncchildadvice","mothernutrition","mothercomplication","pncfollowup","infantcounsellingonbabyhygeine","motherifatablets","infantdandersign","infantcounsellingonweightloss"};



    public static final String[] DOWNLOAD_TABLES_CHILD_CARE = {"childpicture","childfever","childclosure","childexamdpt","childvacdpt_2","childvacdpt_1","childexammeasles","childphyexam","childadvise","childvacopv_0","childadverseevent","childvacopv_1","childvacdptbooster","childsummarynotes","childvacopv_2","childvacpentavalent","childvacopv_3",
            "childregistration","childmalnutrition","childrefer","childvacje","childmeaslessign","childvacdpt_3","childfollowup","childbreastfeeding","childvacmeasles","childvacpoliobooster","childvachepatitisb_1","childvachepatitisb_0","childvisitdetail","childvachepatitisb_3","childgpslocation","childvachepatitisb_2",
            "childvacbcg","childdiarrhoea","childvacvitamin_a"};


    public static final String[] DOWNLOAD_TABLES_SWASTHYA_SLATE = {"weight","pulse_oximeter","pregnancy_hcg","temperature","urine_protein","hiv","blood_glucose","malaria","haemoglobin","urine_sugar","blood_pressure",
            "cholesterol_test","pt_inr_test","foetal_doppler","syphillis"};

    public static final String[] DOWNLOAD_TABLES_REG = {"geolocation","MedFacilities","patientinfo","regMapping","relation","whrabout"};//Local db table names matching the json names, except "Agreement","idproof","key","language"
    public static final String MyPREFERENCES = "jkdataretrieverPreferences";



    public static final String PNC_APP_DATABASE_PATH_NAME = "/data/data/com.phfi.swasthya.pnc/databases/pnc.db";



    public static final String name = "nameKey";
    public static final String passw = "passwordKey";
    public final static String PACKAGE_NAME = "com.phtt.jkdataretriever";


    public static final String applicationDataBaseName[] =
            {
                    "all application",
                    "Registration",//registration
                    "anc",//anc
                    "inc",//inc
            };
public static final int ALL_APPLICATION = 0;
    public static final int APP_REGISTRATION = 1;
    public static final int APP_ANC = 2;
    public static final int APP_INC = 3;
    public static final int APP_PNC= 4;
    public static final int APP_CHILD_CARE = 5;
    public static final int APP_CHILD_ADOLESCENT = 6;

    public static final String ANC_APP_DATABASE_PATH_NAME = "/data/data/phfi.swasthya.anc/databases/anc";
    public static final String INC_APP_DATABASE_PATH_NAME = "/data/data/phfi.swasthya.inc/databases/inc";
    public static final String REG_APP_DATABASE_PATH = "/data/data/phfi.aht.registration/databases/Registration";
    public static final String CHILD_CARE_APP_DATABASE_PATH = "/data/data/com.phfi.childcare/databases/child_care.db";
    public static final String SWASTHYA_SLATE_APP_DATABASE_PATH = "/data/data/phfi.swasthya.swasthyaslate/databases/swasthv2";

    public static final String ANC_APP_DATABASE_PATH_NAME_1 = "/data/phfi.swasthya.anc/databases/anc";
}
