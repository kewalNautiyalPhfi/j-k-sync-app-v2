package com.phtt.jkdataretriever.utils;

import java.util.ArrayList;

/**
 * Created by HP on 12-10-2015.
 */
public interface INetResult {

    void onResult(boolean resultFlag, String message);
    void onResult(boolean resultFlag, String message, ArrayList<?> dataList);
    void onResult(boolean resultFlag, String message, Object data);
    void onResult(int id, boolean resultFlag,String message);

}
