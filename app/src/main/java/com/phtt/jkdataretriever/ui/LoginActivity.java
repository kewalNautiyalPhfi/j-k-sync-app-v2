package com.phtt.jkdataretriever.ui;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.phtt.jkdataretriever.R;
import com.phtt.jkdataretriever.database.DatabaseManager;
import com.phtt.jkdataretriever.database.DbConstant;
import com.phtt.jkdataretriever.models.ApplicationDataModel;
import com.phtt.jkdataretriever.network.NetworkConstants;
import com.phtt.jkdataretriever.network.NetworkRequestManager;
import com.phtt.jkdataretriever.utils.ActivityManagerForFinish;
import com.phtt.jkdataretriever.utils.Constants;
import com.phtt.jkdataretriever.utils.INetResult;
import com.phtt.jkdataretriever.utils.NotificationsDisplay;
import com.phtt.jkdataretriever.utils.Utils;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class LoginActivity extends Activity implements INetResult{

	private EditText user, pass;
	private Button mSubmit;
	Button forgot;

	Context mContext;
	Button exit, help;
	static int setpage;
	Dialog contDialog;
	NotificationsDisplay notify;
	Button contDialogbtn1,contDialogbtn2,contDialogBtnClose;
	EditText nameForgot, passwordForgot;
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_MESSAGE = "message";
	public static final String MyPREFERENCES = "RegPreferences";
	TextView textviewStatus;

	public static final String name = "nameKey";
	public static final String passw = "passwordKey";
	public static String clinic = "clinicId";
	private ProgressDialog pDialog;
	public SharedPreferences token_prefs;
	INetResult listener;
	String username=null,password=null;
	SharedPreferences mPref;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar

		setContentView(R.layout.activity_login);
		ActivityManagerForFinish.getInstance().addActivity(this);

		initViews();
		initData();
		setListeners();

	}

	@Override
	public void onBackPressed()
	{
		Utils.getInstance().exitAlert(mContext);
		// code here to show dialog
	}

	private void setListeners() {
		mSubmit.setOnClickListener(m_click);
		exit.setOnClickListener(m_click);


	}
	View.OnClickListener m_click =new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(v==exit){
				Utils.getInstance().exitAlert(mContext);
			}
			if(v==mSubmit){

				username = user.getText().toString();
				password = pass.getText().toString();
				if (username != null && username.length() >= 3 && password != null && password.length() >= 3) {
					if (Utils.getInstance().isInternetAvailable(mContext)) {
						ApplicationDataModel.getInstance().setHwid(username);
						saveLoginPref(username, password);
						Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
						startActivity(intent);
					} else {
						notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
					}
				}else {
					notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.enter_valid_credentials));
				}

//				startLoginProcess();
			}
		}
	};

	private void initData() {
		mContext=this;
		listener = this;
		notify=new NotificationsDisplay(mContext);
		mPref=getApplicationContext().getSharedPreferences(
				Constants.MyPREFERENCES, Context.MODE_WORLD_WRITEABLE);
		if(!mPref.contains(Constants.name)&&!mPref.contains(Constants.passw)) {
			user.setText(mPref.getString(Constants.name,""));
		}

	}

	private void initViews() {
		user = (EditText) findViewById(R.id.username);
		pass = (EditText) findViewById(R.id.password);
		mSubmit = (Button) findViewById(R.id.login);
		exit = (Button) findViewById(R.id.exit);
	}

	private void saveLoginPref(String username, String password) {


		Context myContext;
		try {

			myContext = createPackageContext(Constants.PACKAGE_NAME,
					Context.MODE_WORLD_WRITEABLE);

			SharedPreferences sharedpreferences = myContext.getSharedPreferences(
					Constants.MyPREFERENCES, Context.MODE_WORLD_WRITEABLE);
			Editor editor = sharedpreferences.edit();
			editor.putString(Constants.name, username);
			editor.putString(Constants.passw, password);
			editor.commit();

			/*HashMap<String, String> values = new HashMap<String, String>();
			values.put(DbConstant.COLUMN_2, username);
			values.put(DbConstant.COLUMN_3, password);

			DatabaseManager.getInstance().insertLogin(mContext, DbConstant.URI_TABLE_LOGIN, values);*/

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	private void startLoginProcess() {
		username = user.getText().toString();
		password = pass.getText().toString();

		if (username != null && username.length() >= 3 && password != null && password.length() >= 3) {
			if(!checkDatabaseforLogin(username,password)) {
				if (Utils.getInstance().isInternetAvailable(mContext)) {
					NetworkRequestManager nrm = new NetworkRequestManager(mContext, this);
					Bundle bundle = new Bundle();
					bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, username);
					bundle.putString(NetworkConstants.REQUEST_KEY_PASSWORD, password);

					nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_LOGIN, bundle);

				}else {
					notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
				}

			}
			else {
				Context myContext;
				try {

					myContext = createPackageContext(Constants.PACKAGE_NAME,
							Context.MODE_WORLD_WRITEABLE);

					SharedPreferences sharedpreferences = myContext.getSharedPreferences(
							Constants.MyPREFERENCES, Context.MODE_WORLD_WRITEABLE);
					Editor editor = sharedpreferences.edit();
					editor.putString(Constants.name, username);
					editor.putString(Constants.passw, password);
					editor.commit();

					Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
					startActivity(intent);
					finish();

				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else {
			notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.enter_valid_credentials));
		}
	}


	private boolean checkDatabaseforLogin(String userName, String passWord){
		boolean bool = false;

		String[] arr = DatabaseManager.getInstance().getLoginValue(mContext, DbConstant.URI_TABLE_LOGIN, userName);

		if (arr != null) {
			if ((arr[0].equalsIgnoreCase(userName))	&& (arr[1].equalsIgnoreCase(passWord))) {
//7 and 8
				// login successfull
				Context myContext = null;

				try {
					myContext = createPackageContext(Constants.PACKAGE_NAME, Context.MODE_WORLD_WRITEABLE);

					SharedPreferences sharedpreferences = myContext.getSharedPreferences(
							Constants.MyPREFERENCES,
							Context.MODE_WORLD_READABLE);
					Editor editor = sharedpreferences.edit();
					editor.putString(Constants.name, userName);
					editor.putString(Constants.passw, passWord);
					editor.commit();

				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				bool = true;

				// start new activity
				// applicationController.handleEvent(ApplicationEvents.EVENT_ID_SELECT_OPTION_SCREEN);
				// applicationController.finishCurrentActivity(this);

			} else {

				bool = false;
				// notify.customToast(getBaseContext(), "notify",
				// getString(R.string.LS_INVALID_PASS));
			}

		}
		return bool;
	}


	@Override
	public void onResult(boolean resultFlag, String message) {

	}

	@Override
	public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {

	}

	@Override
	public void onResult(boolean resultFlag, String message, Object data) {
		if(resultFlag==true){

			if(null!=user.getText().toString())
				ApplicationDataModel.getInstance().setHwid(user.getText().toString());

			Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
			startActivity(intent);
			notify.customToast(mContext, getResources().getString(R.string.info), message);
			saveLoginPref(username, password);
//			LoginActivity.this.finish();
//            openSsa();

		}else{
			notify.customToast(mContext, getResources().getString(R.string.info), "Login Unsuccessful");
		}

	}

	@Override
	public void onResult(int id, boolean resultFlag, String message) {

	}
}
