package com.phtt.jkdataretriever.ui;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.phtt.jkdataretriever.R;
import com.phtt.jkdataretriever.models.ApplicationDataModel;
import com.phtt.jkdataretriever.utils.Constants;

public class SplashActivity extends Activity {

    private static long SPLASH_TIME = 4000;

    Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar

        setContentView(R.layout.activity_splash);

        mContext = this;

        new Thread(new Runnable() {
            @Override
            public void run() {

                try{
                    Thread.sleep(SPLASH_TIME);
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    /*mPref=getApplicationContext().getSharedPreferences(
                            Constants.MyPREFERENCES, Context.MODE_WORLD_WRITEABLE);
                    if(!mPref.contains(Constants.name)&&!mPref.contains(Constants.passw)) {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        ApplicationDataModel.getInstance().setHwid(mPref.getString(Constants.name,"tel1"));
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    }*/
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        }).start();

    }
}
