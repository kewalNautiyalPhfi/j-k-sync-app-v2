package com.phtt.jkdataretriever.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.phtt.jkdataretriever.R;
import com.phtt.jkdataretriever.models.ApplicationDataModel;
import com.phtt.jkdataretriever.network.NetworkConstants;
import com.phtt.jkdataretriever.network.NetworkRequestManager;
import com.phtt.jkdataretriever.utils.INetResult;
import com.phtt.jkdataretriever.utils.NotificationsDisplay;
import com.phtt.jkdataretriever.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class HomeActivity extends Activity implements INetResult {

    Button anc, pnc, inc, reg, child, swasthyaSlate, exit_btn;
    Context mContext;
    private INetResult listener = this;

    String hid, pswd;
    NotificationsDisplay notify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        mContext = this;

        anc = (Button) findViewById(R.id.ancTile);
        pnc = (Button) findViewById(R.id.pncTile);
        inc = (Button) findViewById(R.id.incTile);
        reg = (Button) findViewById(R.id.reg);
        child = (Button) findViewById(R.id.childExaminationTile);
        swasthyaSlate = (Button) findViewById(R.id.swasthyaSlateButton);
        exit_btn = (Button) findViewById(R.id.exitButton);

       /* anc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (Utils.getInstance().isInternetAvailable(mContext)) {
                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    Bundle app_bundle = new Bundle();
//                    app_bundle.putString(NetworkConstants.APP_NAME, "anc");
                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
//            app_bundle.putString(NetworkConstants.REQUEST_KEY_PASSWORD, pswd);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_DOWNLOAD_ANC, app_bundle);
//                } else {
//                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
//                }

            }
        });
*/

        initData();
        setListeners();


    }

    @Override
    public void onBackPressed() {
        Utils.getInstance().exitAlert(mContext);
    }

    private void initData() {
        hid = ApplicationDataModel.getInstance().getHwid();
    }

    private void setListeners() {
        anc.setOnClickListener(m_click);
        pnc.setOnClickListener(m_click);
        inc.setOnClickListener(m_click);
        reg.setOnClickListener(m_click);
        child.setOnClickListener(m_click);
        swasthyaSlate.setOnClickListener(m_click);


    }

    View.OnClickListener m_click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == anc) {
                if (Utils.getInstance().isInternetAvailable(mContext)) {
                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    Bundle app_bundle = new Bundle();
//            app_bundle.putString(NetworkConstants.APP_NAME, "anc");
                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
//            app_bundle.putString(NetworkConstants.REQUEST_KEY_PASSWORD, pswd);
            nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_DOWNLOAD_ANC, app_bundle);

                } else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                }}else if (v == inc) {
                if (Utils.getInstance().isInternetAvailable(mContext)) {
                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    Bundle app_bundle = new Bundle();
//                    app_bundle.putString(NetworkConstants.APP_NAME, "inc");
                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
//                    app_bundle.putString(NetworkConstants.REQUEST_KEY_PASSWORD, pswd);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_DOWNLOAD_INC, app_bundle);

//                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_INC, app_bundle);
                } else {

                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                }
            } else if (v == pnc) {

                if (Utils.getInstance().isInternetAvailable(mContext)) {
                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    Bundle app_bundle = new Bundle();
//                    app_bundle.putString(NetworkConstants.APP_NAME, "pnc");
                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
//                    app_bundle.putString(NetworkConstants.REQUEST_KEY_PASSWORD, pswd);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_DOWNLOAD_PNC, app_bundle);

                } else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                }
            } else if (v == reg) {
                if (Utils.getInstance().isInternetAvailable(mContext)) {
                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    Bundle app_bundle = new Bundle();
//                    app_bundle.putString(NetworkConstants.APP_NAME, "reg");
                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
//                    app_bundle.putString(NetworkConstants.REQUEST_KEY_PASSWORD, pswd);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_DOWNLOAD_REG, app_bundle);


                } else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                }
            } else if (v == child) {
                if (Utils.getInstance().isInternetAvailable(mContext)) {
                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    Bundle app_bundle = new Bundle();
//                    app_bundle.putString(NetworkConstants.APP_NAME, "reg");
                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
//                    app_bundle.putString(NetworkConstants.REQUEST_KEY_PASSWORD, pswd);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_DOWNLOAD_CHILD_CARE, app_bundle);

                } else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                }
            } else if (v == swasthyaSlate) {
                if (Utils.getInstance().isInternetAvailable(mContext)) {
                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    Bundle app_bundle = new Bundle();
//                    app_bundle.putString(NetworkConstants.APP_NAME, "reg");
                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
//                    app_bundle.putString(NetworkConstants.REQUEST_KEY_PASSWORD, pswd);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_DOWNLOAD_SWASTHYA_SLATE, app_bundle);
                } else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                }
            }
        }
    };

    @Override
    public void onResult(boolean resultFlag, String message) {


    }

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {
        if (resultFlag == true) {
            ArrayList<String> all_imges_path = (ArrayList<String>) dataList;
            new DownloadImageTask(all_imges_path).execute(message);
        }

    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {

    }

    @Override
    public void onResult(int id, boolean resultFlag, String message) {

        if (resultFlag == true && message == "success") {

            NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
            Bundle app_bundle = new Bundle();

            switch (id) {

                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_REGISTRATION:

                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);

                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_REGISTRATION, app_bundle);
                    break;


                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_CHILD_CARE:

                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_CHILD_CARE, app_bundle);
                    break;

                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_PNC:

                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_PNC, app_bundle);
                    break;

                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_ANC:

                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_ANC, app_bundle);
                    break;

                case NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_INC:

                    app_bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, hid);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_IMAGE_DOWNLOAD_INC, app_bundle);
                    break;

                default:
                    break;

            }


        } else {
            Toast.makeText(HomeActivity.this, "No Data", Toast.LENGTH_SHORT).show();
        }


    }

    public void onClick(View view) {
        Utils.getInstance().exitAlert(mContext);
    }


    private class DownloadImageTask extends AsyncTask<String, Void, String> {
        ArrayList<String> image_url;

        public DownloadImageTask(ArrayList<String> all_imges_path) {
            image_url = all_imges_path;
        }

        @Override
        protected String doInBackground(String... params) {
            String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                    "/" + params[0];
            File dir = new File(file_path);
            if (!dir.exists())
                dir.mkdirs();

            for (int i = 0; i < image_url.size(); i++) {
                try {
                    String path = Uri.parse(image_url.get(i)).getLastPathSegment();
                    if(path!=null){
                        File file = new File(dir,path);



                    HttpClient httpClient=new DefaultHttpClient();
                    HttpGet httpGet=new HttpGet(image_url.get(i));
                    HttpResponse httpResponse=httpClient.execute(httpGet);
                    if(httpResponse.getStatusLine().getStatusCode()==200) {
                        HttpEntity httpEntity = httpResponse.getEntity();
                        InputStream inputStream = httpEntity.getContent();
                        FileOutputStream fOut = new FileOutputStream(file);
                        byte[] buffer = new byte[1024];
                        long total = 0;
                        int count;
                        while ((count = inputStream.read(buffer)) != -1) {
                            total += count;
                            fOut.write(buffer, 0, count);
                        }
                        fOut.close();
                        inputStream.close();
                    }
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }catch(Exception e){
                    e.printStackTrace();
                }


            }
            return null;
        }
    }
}