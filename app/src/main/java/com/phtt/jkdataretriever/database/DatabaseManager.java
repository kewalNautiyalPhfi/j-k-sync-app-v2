package com.phtt.jkdataretriever.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.util.HashMap;

public class DatabaseManager {


	private static final String TAG = "DatabaseManagesr";


	private static class SingletonHolder {
		private static final DatabaseManager INSTANCE = new DatabaseManager();
	}

	public static DatabaseManager getInstance() {
		return SingletonHolder.INSTANCE;
	}


	public synchronized void insertLogin(Context mContext, Uri contentURI, HashMap<String, String> value) {

		ContentValues values = new ContentValues();
		//DatabaseConfig.COLUMN_COMMON_VISIT_NO
		values.put(DbConstant.COLUMN_2, value.get(DbConstant.COLUMN_2));
		values.put(DbConstant.COLUMN_3, value.get(DbConstant.COLUMN_3));

		Log.v(TAG, "insertLogin, values: " + values);

		mContext.getContentResolver().insert(contentURI, values);

		values.clear();
		values = null;
		DbUtils.getInstance().copy();

	}


	public synchronized String[] getLoginValue(Context mContext, Uri contentURI, String value) {
		String list[] = null;
		Cursor cursor = null;
		try {
			//		cursor = mContext.getContentResolver().query(contentURI, null, DatabaseConfig.COLUMN_COMMON_LID+"=?",

			cursor = mContext.getContentResolver().query(contentURI, null, DbConstant.COLUMN_2 + "=?",
					new String[]{value}, null /*DatabaseConfig.COLUMN_CHILD_GID+" ASC"*/);


			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToLast();
				list = new String[cursor.getColumnCount()];
				for (int temp = 0; temp < cursor.getColumnCount(); temp++) {

					list[temp] = cursor.getString(temp);


				}

			}
			if (cursor != null) {
				cursor.close();
				cursor = null;
			}
		} catch (IllegalArgumentException e) {
			if (cursor != null) {
				cursor.close();
				cursor = null;
			}
			e.printStackTrace();
		}

		return list;
	}

}
