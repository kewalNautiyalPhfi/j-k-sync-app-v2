package com.phtt.jkdataretriever.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phtt.jkdataretriever.utils.Constants;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class ANCDataBase extends SQLiteOpenHelper{

	private static final int Database_Version = 1;// variable that defines the
	private static final String Database_Name = "anc_temp";// setting Regdatabase name
	public static ArrayList<String> listPatientId = new ArrayList<String>();// an
//	public  String [] track;
	public  String [] countTest;
	private static final String Tablemap = "regMapping";
	public boolean found = false;
	
	
	
	public static String[] tablename = {"anc_ancvisitdetails","anc_followup","anc_nutrition","anc_substance", "anc_ancmothersdetails", "anc_basichealth", "anc_birthplan","anc_contraception","anc_familyhistory","anc_fetal","anc_finances","anc_gac","anc_generalhealth","anc_gynecologyhistory","anc_laborsign","anc_prevobstetrichistory","anc_psychosocial","anc_referral","anc_symptomatic", "anc_maternaldeath","anc_picture","anc_gps","anc_advice"};
	private static final String ancmothersdetails = "anc_ancmothersdetails";
	private static final String ancvisitdetails = "anc_ancvisitdetails";
	private static final String basicHealth = "anc_basichealth";
	private static final String birthPlan = "anc_birthplan";
	private static final String contraception = "anc_contraception";
	private static final String familyHistory = "anc_familyhistory";
	private static final String fetal = "anc_fetal";
	private static final String finances = "anc_finances";
	private static final String followUp = "anc_followup";
	private static final String gac = "anc_gac";
	private static final String generalHealth = "anc_generalhealth";
	private static final String gynecologyHistory = "anc_gynecologyhistory";
	private static final String laborSign = "anc_laborsign";
	private static final String nutrition = "anc_nutrition";
	private static final String prevobstetrichistory = "anc_prevobstetrichistory";
	private static final String psychosocial = "anc_psychosocial";
	private static final String referral = "anc_referral";
	private static final String substance = "anc_substance";
	private static final String symptomatic = "anc_symptomatic";
	private static final String maternalDeath = "anc_maternaldeath";
	private static final String noteSummary = "anc_Notes";
	private static final String motherpicture = "anc_picture";
	private static final String gps = "anc_gps";
	private static final String compHistory = "anc_comphistory";
	private static final String TB_ADVICE = "anc_advice";




	private String COLUMN_GID =  "gid";
	private String COLUMN_ID =  "id";
	private String COLUMN_NUT = "nut";
	private String COLUMN_FH = "fh";
	private String COLUMN_SYM= "sym";
	private String COLUMN_OH = "oh";
	private String COLUMN_BH =  "bh";
	private String COLUMN_GH =  "gh";
	private String COLUMN_GC = "gc";
	private String COLUMN_FETAL = "fetal";
	private String COLUMN_ANC_VISIT = "anc_visit";
	private String COLUMN_BPLAN = "bplan";
	private String COLUMN_GPS= "gps";
	private String COLUMN_PICS = "pics";
	private String COLUMN_DATE = "datetime";
	private String COLUMN_EDITFLAG = "editFlag";
	private String COLUMN_ANC_UPFLAG = "upFlag";


	private String TABLE_COMP_HISTORY = "anc_comphistory";

	String tableName;
	SQLiteDatabase ancDB;

	public ANCDataBase(Context context,String tabName) {
		super(context, Database_Name, null, Database_Version);

		ancDB = SQLiteDatabase.openDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);
		this.tableName=tabName;
		if(ancDB!=null && tableName!=null && !tableName.isEmpty())
		ancDB.delete(tableName, null, null);
		ancDB.close();

		// TODO Auto-generated constructor stub
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub


	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	/*
	 * Method to copy the Database form the internal memory to the external memory
	 */
	public void copy() {

		FileChannel src = null;
		FileChannel dest = null;
		try {
			// set file object to the external path of the Database
			File sd = new File(Environment.getExternalStorageDirectory()
					.toString() + "/swasthyaslate/");
			File data = Environment.getDataDirectory();// gets the android data directory
			sd.mkdirs();// make directory to the path specified to the object

			String currentDb = Constants.ANC_APP_DATABASE_PATH_NAME_1;// location of current Database in the internal memory
			String backupDb = "anc";//name of the Database in the external memory
			File curDb = new File(data, currentDb);// file object that specifies the internal Database location

			File backup = new File(sd, backupDb);// File object that specifies the location of the external memory Database

			if (curDb.exists()) {

				src = new FileInputStream(curDb).getChannel();// Source channel of the Database
				dest = new FileOutputStream(backup).getChannel();// destination channel of the Database
				dest.transferFrom(src, 0, src.size());// coppies data from source to the destination


			}
		} catch (Exception e) {
			// TODO
			Log.e("Exception", "copy()", e);
		}
		finally{
			try{
				src.close();//close object
				dest.close();//close destination object
			}
			catch(Exception e){

			}
		}
	}

	/*public synchronized void addDataAnc(ContentValues valuesParams)throws SQLiteException{
//		SQLiteDatabase d = this.getWritableDatabase();
		try{
//		SQLiteDatabase d = SQLiteDatabase.openOrCreateDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null);


		ancDB = SQLiteDatabase.openDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);
		ContentValues values = new ContentValues();
		ArrayList<String> list = new ArrayList<String>();

		values = valuesParams;





			values.put("upflag", 1);
			values.put("editFlag", 1);
//			ancDB.delete(tableName, null, null);
			long i= ancDB.insert(tableName, null, values);
			//Log.d("Data_inserted"+i+":",""+String.valueOf(values));

			if(tableName.equalsIgnoreCase(ancvisitdetails)) {
				ContentValues Newvalue = null;



					Newvalue = new ContentValues();
					Newvalue.put(COLUMN_GID, values.getAsString("gid"));
					Newvalue.put(COLUMN_ID, values.getAsString("id"));
					Newvalue.put(COLUMN_ANC_VISIT, values.getAsString("anc_visit"));
					Newvalue.put(COLUMN_NUT, 1);
					Newvalue.put(COLUMN_FH, 1);
					Newvalue.put(COLUMN_SYM, 1);
					Newvalue.put(COLUMN_NUT, 1);
					Newvalue.put(COLUMN_OH, 1);
					Newvalue.put(COLUMN_BH, 1);
					Newvalue.put(COLUMN_GH, 1);
					Newvalue.put(COLUMN_GC, 1);
					Newvalue.put(COLUMN_FETAL, 1);

					Newvalue.put(COLUMN_BPLAN, 1);
					Newvalue.put(COLUMN_GPS, 1);
					Newvalue.put(COLUMN_PICS, 1);
					Newvalue.put(COLUMN_DATE, values.getAsString("datetime"));
					Newvalue.put(COLUMN_EDITFLAG, values.getAsString("editFlag"));
					Newvalue.put(COLUMN_ANC_UPFLAG, values.getAsString("upflag"));

				//if (values.get("anc_visit")!=null && values.get("gid")!= null) {


					ancDB.insert(TABLE_COMP_HISTORY, null, Newvalue);


				//}

			}

			copy();

		//this.copy(incDB);
		}
		catch (SQLiteException e) {
			// TODO: handle exception
			Log.d("error", e.getMessage());
		}
	}*/

	public synchronized void addDataAnc(ContentValues valuesParams)throws SQLiteException{
//		SQLiteDatabase d = this.getWritableDatabase();
		try{
//		SQLiteDatabase d = SQLiteDatabase.openOrCreateDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null);


			ancDB = SQLiteDatabase.openDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);
			ContentValues values = new ContentValues();
			ArrayList<String> list = new ArrayList<String>();

			values = valuesParams;




/*
		Cursor ti = incDB.rawQuery("PRAGMA table_info("+TableName+")", null);

    	if ( ti.moveToFirst() ) {
    	    do {
    	    	list.add(ti.getString(1));
    	        System.out.println("col: " + ti.getString(1));
    	    } while (ti.moveToNext());
    	}

		  for(Iterator iteratora =  mJsonObject.keys(); iteratora.hasNext();) {
			    String key = (String) iteratora.next();


			    if (!key.equalsIgnoreCase("id")) {

					try {
						if (list.contains(key)) {
							String valuesAnc = mJsonObject.getString(key);
					    	values.put(key, valuesAnc);
						}


					} catch (JSONException e) {
						//
						TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}*/
			values.put("upflag", 1);
			values.put("editFlag", 1);
//			ancDB.delete(tableName, null, null);
			long i= ancDB.insert(tableName, null, values);
			//Log.d("Data_inserted"+i+":",""+String.valueOf(values));


			ancDB.close();
			copy();
			//this.copy(incDB);
		}
		catch (SQLiteException e) {
			// TODO: handle exception
			Log.d("error", e.getMessage());
		}
	}

	/*vals=table name*/
	public void AddAncDb(HashMap<String,JSONArray> allVals,String[] vals){
		JSONArray[] tempArr = new JSONArray[vals.length];
		ancDB = SQLiteDatabase.openDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);

		for(int i=0; i<vals.length;i++){
			tempArr[i]=allVals.get(vals[i]);
			JSONObject tempJObj=null;
			try {
				tempJObj=tempArr[i].getJSONObject(i);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if(tempJObj!=null) {
				HashMap<String, JSONArray> tempMap = getAllVals(tempJObj);
/*
				for (String key : tempMap.keySet()) {
					System.out.println("key: " + key + " value: " + tempMap.get(key));
					*/
/*Insert into db*//*

					long res=incDB.insert(vals[i], null, tempMap.get(key));

				}
*/


			}
			/*Inserting element by element into db*/
		}


	}
	private HashMap<String,JSONArray> getAllVals (JSONObject jsonObject) {
		HashMap<String,JSONArray> keyValPair = null;

		Iterator a = jsonObject.keys();
		while(a.hasNext()) {
			String key = (String)a.next();
			// loop to get the dynamic key
			JSONArray value = null;
			try {
				value = (JSONArray)jsonObject.get(key);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			keyValPair.put(key,value);
		}
		return  keyValPair;
	}
}
