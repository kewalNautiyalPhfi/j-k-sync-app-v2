package com.phtt.jkdataretriever.database;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.phtt.jkdataretriever.models.PNCDatabaseConfig;
import com.phtt.jkdataretriever.utils.Constants;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class PncDatabase extends SQLiteOpenHelper{

	
	private final String TAG = "PncDatabase";
	public static  String [] track;
	public volatile boolean found = false;
	private SQLiteDatabase database = null;
	
	PncDatabase instance;

	public PncDatabase(Context con, String tableName)
	{
		super(con, PNCDatabaseConfig.DATABASE_NAME, null, PNCDatabaseConfig.DATABASE_VERSION);
		//this.getReadableDatabase();

	}
	

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	/*
	 * Method to copy the Database form the internal memory to the external memory
	 */
	public void copy() {

		FileChannel src = null;
		FileChannel dest = null;
		try {
			// set file object to the external path of the Database
			File sd = new File(Environment.getExternalStorageDirectory()
					.toString() + "/swasthyaslate/");
			File data = Environment.getDataDirectory();// gets the android data directory
			sd.mkdirs();// make directory to the path specified to the object
			//db.openDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null, db.OPEN_READWRITE);
//            String currentDb = Constants.REG_APP_DATABASE_PATH;// location of current Database in the internal memory
			String backupDb = "pnc.db";//name of the Database in the external memory
			File curDb = new File(Constants.PNC_APP_DATABASE_PATH_NAME);// file object that specifies the internal Database location

			File backup = new File(sd, backupDb);// File object that specifies the location of the external memory Database

			if (curDb.exists()) {

				src = new FileInputStream(curDb).getChannel();// Source channel of the Database
				dest = new FileOutputStream(backup).getChannel();// destination channel of the Database
				dest.transferFrom(src, 0, src.size());// coppies data from source to the destination


			}
		} catch (Exception e) {
			// TODO
			Log.e("Exception", "copy()", e);
		}
		finally{
			try{
				src.close();//close object
				dest.close();//close destination object
			}
			catch(Exception e){

			}
		}
	}
	public synchronized void addDataPNC(ContentValues values,  String TableName){
		try{


		SQLiteDatabase d = SQLiteDatabase.openDatabase(Constants.PNC_APP_DATABASE_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);
//		ContentValues values = new ContentValues();
		ArrayList<String> list = new ArrayList<String>();

		if(values.containsKey("server_date")){
			values.remove("server_date");
		}

		String tableName ="";

		switch (TableName){
			case DbConstant.TABLE_MOTHER_POSTPARTUM_CARE_N_HYGEINE:
            tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_POSTPARTUM_CARE_N_HYGEINE;
				break;

			case DbConstant.TABLE_MOTHER_NUTRITION:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_NUTRITION;
				break;

			case DbConstant.TABLE_MOTHER_CONTRACEPTION:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_CONTRACEPTION;
				break;

			case DbConstant.TABLE_MOTHER_BREASTFEEDING:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_BREASTFEEDING;
				break;

			case DbConstant.TABLE_REGISTRATIONOF_BIRTH:
				tableName= PNCDatabaseConfig.getInstance().TABLE_REGISTRATIONOF_BIRTH;
				break;

			case DbConstant.TABLE_MOTHER_IFA_TABLETS:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_IFA_TABLETS;
				break;

			case DbConstant.TABLE_MOTHER_DANGER_SIGN:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_DANGER_SIGN;
				break;

			case DbConstant.TABLE_MOTHER_COMPLICATION:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_COMPLICATION;
				break;

			case DbConstant.TABLE_MOTHER_PHYSICAL_EXAMINATION:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_PHYSICAL_EXAMINATION;
				break;

			case DbConstant.TABLE_MOTHER_COUNSELLING_ON_BREASTFEEDING:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_COUNSELLING_ON_BREASTFEEDING;
				break;

			case DbConstant.TABLE_MOTHER_COUNSELLING_REGARDING_INFANT_CARE:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_COUNSELLING_REGARDING_INFANT_CARE;
				break;

			case DbConstant.TABLE_INFANT_DANDER_SIGN:
				tableName= PNCDatabaseConfig.getInstance().TABLE_INFANT_DANDER_SIGN;
				break;

			case DbConstant.TABLE_INFANT_COMPLICATIONS:
				tableName= PNCDatabaseConfig.getInstance().TABLE_INFANT_COMPLICATIONS;
				break;

			case DbConstant.TABLE_INFANT_COUNSELLING_ON_WEIGHTLOSS:
				tableName= PNCDatabaseConfig.getInstance().TABLE_INFANT_COUNSELLING_ON_WEIGHTLOSS;
				break;

			case DbConstant.TABLE_INFANT_COUNSELLING_ON_BABY_HYGEINE:
				tableName= PNCDatabaseConfig.getInstance().TABLE_INFANT_COUNSELLING_ON_BABY_HYGEINE;
				break;

			case DbConstant.TABLE_INFANT_PHYSICAL_EXAMINATION:
				tableName= PNCDatabaseConfig.getInstance().TABLE_INFANT_PHYSICAL_EXAMINATION;
				break;

			case DbConstant.TABLE_REFERRALOF_MOTHERAND_INFANT:
				tableName= PNCDatabaseConfig.getInstance().TABLE_REFERRALOF_MOTHERAND_INFANT;
				break;

			case DbConstant.TABLE_INFANT_DEATH:
				tableName= PNCDatabaseConfig.getInstance().TABLE_INFANT_DEATH;
				break;

			case DbConstant.TABLE_MOTHER_DEATH:
				tableName= PNCDatabaseConfig.getInstance().TABLE_MOTHER_DEATH;
				break;

			case DbConstant.TABLE_PNC_REGISTRATION:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_REGISTRATION;
				break;

			case DbConstant.TABLE_PNC_PICTURE:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_PICTURE;
				break;

			case DbConstant.TABLE_PNC_GPS_LOCATION:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_GPS_LOCATION;
				break;

			case DbConstant.TABLE_PNC_HISTORY:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_HISTORY;
				break;

			case DbConstant.TABLE_PNC_REFERRAL:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_REFERRAL;
				break;

			case DbConstant.TABLE_PNC_FOLLOWUP:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_FOLLOWUP;
				break;

			case DbConstant.TABLE_PNC_SUMARRY_NOTES:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_SUMARRY_NOTES;
				break;

			case DbConstant.TABLE_PNC_ADVICE:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_ADVICE;
				break;

			case DbConstant.TABLE_PNC_REFERRAL_CHILD:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_REFERRAL_CHILD;
				break;

			case DbConstant.TABLE_PNC_FOLLOWUP_CHILD:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_FOLLOWUP_CHILD;
				break;

			case DbConstant.TABLE_PNC_ADVICE_CHILD:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_ADVICE_CHILD;
				break;

			case DbConstant.TABLE_PNC_SUMARRY_NOTES_CHILD:
				tableName= PNCDatabaseConfig.getInstance().TABLE_PNC_SUMARRY_NOTES_CHILD;
				break;




	      default:
			  break;

		}
		if(!tableName.isEmpty()){

			long i=d.insert(tableName, null, values);
		this.copy();
		}
		d.close();

		}catch (Exception e){
			e.printStackTrace();
		}
	}
}
