package com.phtt.jkdataretriever.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.phtt.jkdataretriever.utils.Constants;

public class INCDataBase extends SQLiteOpenHelper{

	private static final String TABLEMAP = "regMapping";
	private static final String TB_PICTURE = "inc_picture";
	private static final String TB_GPS = "inc_gps";
	private static final String TB_FOLLOW = "inc_follow";
	private static final String TB_REFERRAL = "inc_referral";
	private static final String TB_INCSTATUS = "inc_status";
	private static final String TB_COMP_HISTORY = "inc_comphistory";
	private static final String TB_ADVICE = "inc_advice";
	private static final String TB_NOTES = "inc_Notes";
	private static final String[] TABLE_NAME_LIST = {"inc_breastfeeding","inc_atbirth","inc_delivery","inc_complication","inc_doctor","inc_newborninfo","inc_newborndefects","inc_newbornimmunization"};
	private static final int DATABASE_VERSION = 1;// variable that defines the
	private static final String DATABASE_NAME = "inc_temp";// setting Regdatabase name
//	public static String[] tableName = {"inc_status","inc_referral","inc_picture",};

	public  String [] track;
	public boolean found = false;
	String tableName;
	SQLiteDatabase incDB;
	
	public INCDataBase(Context context,String tabName) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		incDB = SQLiteDatabase.openDatabase(Constants.INC_APP_DATABASE_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);
		this.tableName=tabName;
		if(incDB!=null && tableName!=null && !tableName.isEmpty())
		incDB.delete(tableName, null, null);
		incDB.close();
//SELECT tableName FROM inc;
		// TODO Auto-generated constructor stub
	}
	@Override
	public void onCreate(SQLiteDatabase db) {

		/*String create;

		for(int i=0; i<TABLE_NAME_LIST.length;i++){
			create = "Create Table "+ TABLE_NAME_LIST[i] + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT,`gid` TEXT,`col1` TEXT,`col2` TEXT,`col3` TEXT,`col4` TEXT,`col5` TEXT,`col6` TEXT,`col7` TEXT,`col8` TEXT,`col9` TEXT,`col10` TEXT,`col11` TEXT,`col12` TEXT,`col13` TEXT,`col14` TEXT,`col15` TEXT,`col16` TEXT,`col17` TEXT,`col18` TEXT,`col19` TEXT,`col20` TEXT, `category` TEXT ,`datetime` TEXT, `hwid` TEXT, `upflag` INTEGER, `editFlag` INTEGER)";
			db.execSQL(create); 
		}


		String create1 = "Create Table "+ TB_FOLLOW + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT, `gid` TEXT, `hwid` TEXT, `followup` TEXT, `followUpTime` TEXT, `date` TEXT, `category` TEXT, `module_name` TEXT, `datetime` TEXT, `upFlag` INTEGER, `editFlag` INTEGER)";
		db.execSQL(create1);

		String create2 = "Create Table "+ TB_REFERRAL + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT, `gid` TEXT, `reason` TEXT, `hwid` TEXT, `referto` TEXT, `module_name` TEXT, `category` TEXT, `date` TEXT, `datetime` TEXT, `upflag` INTEGER, `editFlag` INTEGER)";
		db.execSQL(create2);

		String create3 = "Create Table "+ TB_PICTURE + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT, `gid` TEXT, `sidepic` TEXT, `frontpic` TEXT, `datetime` TEXT, `upFlag` INTEGER, `editFlag` INTEGER)";
		db.execSQL(create3);

		String create4 = "Create Table "+ TB_GPS + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT, `gid` TEXT, `alat` TEXT, `along` TEXT, `mlat` TEXT, `mlong` TEXT, `datetime` TEXT, `upFlag` INTEGER, `editFlag` INTEGER)";
		db.execSQL(create4);

		String create5 = "Create Table "+ TB_INCSTATUS + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT,`gid` TEXT,`name` TEXT, `category` TEXT,`referFlag` INTEGER,`followFlag` INTEGER, `datetime` TEXT, `hwid` TEXT, `upflag` INTEGER, `editFlag` INTEGER)";
		db.execSQL(create5);

		String create6 = "Create Table "+ TB_COMP_HISTORY + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT,`gid`  TEXT, `inc_breastfeeding` INTEGER, `inc_delivery` INTEGER, `inc_doctor` INTEGER, `inc_complication` INTEGER, `inc_atbirth` INTEGER, `inc_newbornimmunization` INTEGER, `inc_newborninfo` INTEGER, `inc_newborndefects` INTEGER, `inc_gps` INTEGER, `inc_picture` INTEGER, `datetime`  TEXT, `editFlag` INTEGER, `upFlag` INTEGER)";
		db.execSQL(create6);

		String create7 = "CREATE TABLE `"+TB_ADVICE+"` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `gid` TEXT, `advice` TEXT, `module_name` TEXT, `datetime` TEXT, `upFlag` INTEGER, `editFlag`	INTEGER);";
		db.execSQL(create7);
		
		String create8 = "Create Table "+ TB_NOTES + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT, `gid` TEXT, `notes` TEXT, `datetime` TEXT, `upFlag` INTEGER, `editFlag` INTEGER)";
		db.execSQL(create8);
		
		copy();*/
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	
	/*
	 * Method to copy the Database form the internal memory to the external memory
	 */
	public void copy() {

		FileChannel src = null;
		FileChannel dest = null;
		try {
			// set file object to the external path of the Database
			File sd = new File(Environment.getExternalStorageDirectory()
					.toString() + "/swasthyaslate/");
			File data = Environment.getDataDirectory();// gets the android data directory
			sd.mkdirs();// make directory to the path specified to the object

			String currentDb = "/data/data/phfi.swasthya.inc/databases/inc";// location of current Database in the internal memory
			String backupDb = "inc";//name of the Database in the external memory
			File curDb = new File(data, currentDb);// file object that specifies the internal Database location

			File backup = new File(sd, backupDb);// File object that specifies the location of the external memory Database

			if (curDb.exists()) {

				src = new FileInputStream(curDb).getChannel();// Source channel of the Database 
				dest = new FileOutputStream(backup).getChannel();// destination channel of the Database
				dest.transferFrom(src, 0, src.size());// coppies data from source to the destination


			}
		} catch (Exception e) {
			// TODO 
			Log.e("Exception", "copy()", e);	
		}
		finally{
			try{
				src.close();//close object
				dest.close();//close destination object
			}
			catch(Exception e){

			}
		}
	}
	
	public synchronized void addDataInc(ContentValues values){
		try{

			incDB = SQLiteDatabase.openDatabase(Constants.INC_APP_DATABASE_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);

//            values.put("upflag", 1);
//            values.put("editFlag", 1);

//            regDB.delete(tableName, null, null);
			long i = 0;
			if(tableName!=null && values !=null) {
				i = incDB.insert(tableName, null, values);
			}

			Log.d("Data_inserted "+i,""+String.valueOf(values));


		} catch (Exception e) {
			// TODO
			Log.e("Exception", "copy()", e);
		}

		copy();
		incDB.close();
		//this.copy(incDB);

	}
}
