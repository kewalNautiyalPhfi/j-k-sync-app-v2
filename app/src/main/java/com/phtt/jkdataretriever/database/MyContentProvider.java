package com.phtt.jkdataretriever.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

public class MyContentProvider extends ContentProvider {
	
	
	
	private static final String TAG = "MyContentProvider";

	public static UriMatcher uriMatcher;
	

	private static DatabaseHelper DATABASE_INSTANCE;

	/**
	 * DECLARATION OF URI MACHER FOR ALL TABLES
	 */
	static {

		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

		for(int i =0;i<DbConstant.ALL_TABLES.length;i++){

			uriMatcher.addURI(DbConstant.AUTHORITY, DbConstant.ALL_TABLES[i], DbConstant.ALL_TABLE_ID[i]);

		}
	}

	public static DatabaseHelper getSQLiteDatabaseObject(){
		return MyContentProvider.DATABASE_INSTANCE;
	}
	
	public static DatabaseHelper getDatabaseSQLiteOpenHelper(final Context ctx)
	{
		if (null == MyContentProvider.DATABASE_INSTANCE)
		{
			MyContentProvider.DATABASE_INSTANCE = new DatabaseHelper(ctx);
		}
		return MyContentProvider.DATABASE_INSTANCE;
	}

	
	@Override
	public boolean onCreate() {
		MyContentProvider.getDatabaseSQLiteOpenHelper(this.getContext())
		.getWritableDatabase();

		Log.i(TAG, "onCreate() called");

		return false;
	}
	
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

		Cursor mCursor = null;

		final SQLiteDatabase db = MyContentProvider.getDatabaseSQLiteOpenHelper(getContext()).getWritableDatabase();

		switch (MyContentProvider.uriMatcher.match(uri)) {
		
		case DbConstant.ID_TABLE_LOGIN:
			mCursor = db.query(DbConstant.TABLE_LOGIN, projection,selection, selectionArgs, null, null, sortOrder);
		break;
	
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);
		Log.v(TAG, "query is successful");

		return mCursor;
	}
	
	
	
	@Override
	public Uri insert(Uri uri, ContentValues values) {

		Uri returnUri =null;
		final SQLiteDatabase db = MyContentProvider.getDatabaseSQLiteOpenHelper(getContext()).getWritableDatabase();

		switch (MyContentProvider.uriMatcher.match(uri)) {
			
		case DbConstant.ID_TABLE_LOGIN:
			returnUri = Uri.withAppendedPath(uri, String.valueOf(db.insert(DbConstant.TABLE_LOGIN, null, values)));
						break;
		
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);

		Log.i(TAG, "data inserted successfully...");



		return returnUri;
	}
	
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {

		int rowsDeleted = 0;

		final SQLiteDatabase db = MyContentProvider.getDatabaseSQLiteOpenHelper(getContext()).getWritableDatabase();

		switch(MyContentProvider.uriMatcher.match(uri))
		{
		
		case DbConstant.ID_TABLE_LOGIN:
			rowsDeleted = db.delete(DbConstant.TABLE_LOGIN, selection,selectionArgs);
		break;

		
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);

		Log.v(TAG, "row deleted :"+rowsDeleted);

		return rowsDeleted;
	}
	
	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		int rowsUpdated = 0;

		final SQLiteDatabase db = MyContentProvider.getDatabaseSQLiteOpenHelper(getContext()).getWritableDatabase();

		switch(MyContentProvider.uriMatcher.match(uri))
		{
		case DbConstant.ID_TABLE_LOGIN:
			rowsUpdated = db.update(DbConstant.TABLE_LOGIN,values, selection,selectionArgs);
		break;
						

		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);

		Log.v(TAG, "row updated :"+rowsUpdated );
		return rowsUpdated;
	}
	

	@Override
	public String getType(Uri uri) {
		return null;
	}


}
