package com.phtt.jkdataretriever.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.phtt.jkdataretriever.models.PNCDatabaseConfig;
import com.phtt.jkdataretriever.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Sharma on 02-02-2016.
 */
public class ChildCareDataBase  extends SQLiteOpenHelper {
    private static final int Database_Version = 1;// variable that defines the
    private static final String Database_Name = "child_temp";// setting Regdatabase name
    SQLiteDatabase childDB;

    public ChildCareDataBase(Context mContext,String tableName) {
        super(mContext, Database_Name, null, Database_Version);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        childDB = SQLiteDatabase.openDatabase(Constants.CHILD_CARE_APP_DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void copy() {

        FileChannel src = null;
        FileChannel dest = null;
        try {
            // set file object to the external path of the Database
            File sd = new File(Environment.getExternalStorageDirectory()
                    .toString() + "/swasthyaslate/");
            File data = Environment.getDataDirectory();// gets the android data directory
            sd.mkdirs();// make directory to the path specified to the object
            //db.openDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null, db.OPEN_READWRITE);
//            String currentDb = Constants.REG_APP_DATABASE_PATH;// location of current Database in the internal memory
            String backupDb = "child_care";//name of the Database in the external memory
            File curDb = new File(Constants.CHILD_CARE_APP_DATABASE_PATH);// file object that specifies the internal Database location

            File backup = new File(sd, backupDb);// File object that specifies the location of the external memory Database

            if (curDb.exists()) {

                src = new FileInputStream(curDb).getChannel();// Source channel of the Database
                dest = new FileOutputStream(backup).getChannel();// destination channel of the Database
                dest.transferFrom(src, 0, src.size());// coppies data from source to the destination


            }
        } catch (Exception e) {
            // TODO
            Log.e("Exception", "copy()", e);
        }
        finally{
            try{
                src.close();//close object
                dest.close();//close destination object
            }
            catch(Exception e){

            }
        }
    }



    public synchronized void addDataChildCare(ContentValues values,  String TableName){
        SQLiteDatabase d = SQLiteDatabase.openDatabase(Constants.CHILD_CARE_APP_DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
//        ContentValues values = new ContentValues();
        ArrayList<String> list = new ArrayList<String>();

        if(values.containsKey("serverdate")){
            values.remove("serverdate");
        }
        String tableName ="";

        switch (TableName){
            case DbConstant.TABLE_CHILD_REGISTRATION:
                tableName= ChildCareConstants.getInstance().TABLE_CHILD_REGISTRATION;
                break;

            case DbConstant.TABLE_CHILD_PICTURE:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_PICTURE;

                break;

            case DbConstant.TABLE_CHILD_GPS_LOCATOIN:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_GPS_LOCATOIN      ;
                break;

            case DbConstant.TABLE_CHILD_CLOSURE:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_CLOSURE           ;
                break;

            case DbConstant.TABLE_CHILD_BREASTFEEDING:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_BREASTFEEDING     ;
                break;

            case DbConstant.TABLE_CHILD_EXAM_MEASLES:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_EXAM_MEASLES      ;
                break;

            case DbConstant.TABLE_CHILD_EXAM_DPT:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_EXAM_DPT          ;
                break;

            case DbConstant.TABLE_CHILD_ADVERSE_EVENT:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_ADVERSE_EVENT     ;
                break;

            case DbConstant.TABLE_CHILD_VAC_BCG:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_BCG           ;
                break;

            case DbConstant.TABLE_CHILD_VAC_OPV_0:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_OPV_0         ;
                break;

            case DbConstant.TABLE_CHILD_VAC_HEPA_B_0:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_HEPA_B_0      ;
                break;

            case DbConstant.TABLE_CHILD_VAC_OPV_1:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_OPV_1         ;
                break;

            case DbConstant.TABLE_CHILD_VAC_OPV_2:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_OPV_2         ;
                break;

            case DbConstant.TABLE_CHILD_VAC_OPV_3:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_OPV_3         ;
                break;

            case DbConstant.TABLE_CHILD_VAC_DPT_1:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_DPT_1         ;
                break;

            case DbConstant.TABLE_CHILD_VAC_DPT_2:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_DPT_2         ;
                break;

            case DbConstant.TABLE_CHILD_VAC_DPT_3:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_DPT_3         ;
                break;

            case DbConstant.TABLE_CHILD_VAC_HEPA_B_1:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_HEPA_B_1      ;
                break;

            case DbConstant.TABLE_CHILD_VAC_HEPA_B_2:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_HEPA_B_2      ;
                break;

            case DbConstant.TABLE_CHILD_VAC_HEPA_B_3:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_HEPA_B_3      ;
                break;

            case DbConstant.TABLE_CHILD_VAC_MEASLES:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_MEASLES       ;
                break;

            case DbConstant.TABLE_CHILD_VAC_VITAMIN_A:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_VITAMIN_A     ;
                break;

            case DbConstant.TABLE_CHILD_VAC_DPT_BOOSTER:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_DPT_BOOSTER   ;
                break;

            case DbConstant.TABLE_CHILD_VAC_POLIO_BOOSTER:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_POLIO_BOOSTER ;
                break;

            case DbConstant.TABLE_CHILD_REFER:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_REFER             ;
                break;

            case DbConstant.TABLE_CHILD_FOLLOW_UP:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_FOLLOW_UP         ;
                break;

            case DbConstant.TABLE_CHILD_DETAILS:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_DETAILS           ;
                break;

            case DbConstant.TABLE_CHILD_DIARRHOEA:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_DIARRHOEA         ;
                break;

            case DbConstant.TABLE_CHILD_PHY_EXAM:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_PHY_EXAM          ;
                break;

            case DbConstant.TABLE_CHILD_FEVER:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_FEVER             ;
                break;

            case DbConstant.TABLE_CHILD_MEASLES_SIGN:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_MEASLES_SIGN      ;
                break;

            case DbConstant.TABLE_CHILD_SUMMARY_NOTES:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_SUMMARY_NOTES     ;
                break;

            case DbConstant.TABLE_CHILD_ADVISE:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_ADVISE            ;
                break;

            case DbConstant.TABLE_CHILD_VAC_JE:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_JE            ;
                break;

            case DbConstant.TABLE_CHILD_VAC_PENTAVALENT:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_VAC_PENTAVALENT   ;
                break;

            case DbConstant.TABLE_CHILD_MALNUTRITION:
                tableName= ChildCareConstants.getInstance(). TABLE_CHILD_MALNUTRITION		;
                break;


            default:
                break;

        }
        if(!tableName.isEmpty()){

            long i=d.insert(tableName, null, values);
            this.copy();
        }
        d.close();

    }






}