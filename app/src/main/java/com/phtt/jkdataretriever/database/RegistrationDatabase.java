package com.phtt.jkdataretriever.database;

import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by HP on 1/29/2016.
 */
public class RegistrationDatabase {

    private static final int Database_Version = 1;// variable that defines the
    private static final String Database_Name = "reg_temp";// setting Regdatabase name
    public static ArrayList<String> listPatientId = new ArrayList<String>();// an
    //	public  String [] track;
    public  String [] countTest;
    private static final String Tablemap = "regMapping";
    public boolean found = false;



    public static String[] tablename = {"anc_ancvisitdetails","anc_followup","anc_nutrition","anc_substance", "anc_ancmothersdetails", "anc_basichealth", "anc_birthplan","anc_contraception","anc_familyhistory","anc_fetal","anc_finances","anc_gac","anc_generalhealth","anc_gynecologyhistory","anc_laborsign","anc_prevobstetrichistory","anc_psychosocial","anc_referral","anc_symptomatic", "anc_maternaldeath","anc_picture","anc_gps","anc_advice"};
    private static final String ancmothersdetails = "anc_ancmothersdetails";
    private static final String ancvisitdetails = "anc_ancvisitdetails";
    private static final String basicHealth = "anc_basichealth";
    private static final String birthPlan = "anc_birthplan";
    private static final String contraception = "anc_contraception";
    private static final String familyHistory = "anc_familyhistory";
    private static final String fetal = "anc_fetal";
    private static final String finances = "anc_finances";
    private static final String followUp = "anc_followup";
    private static final String gac = "anc_gac";
    private static final String generalHealth = "anc_generalhealth";
    private static final String gynecologyHistory = "anc_gynecologyhistory";
    private static final String laborSign = "anc_laborsign";
    private static final String nutrition = "anc_nutrition";
    private static final String prevobstetrichistory = "anc_prevobstetrichistory";
    private static final String psychosocial = "anc_psychosocial";
    private static final String referral = "anc_referral";
    private static final String substance = "anc_substance";
    private static final String symptomatic = "anc_symptomatic";
    private static final String maternalDeath = "anc_maternaldeath";
    private static final String noteSummary = "anc_Notes";
    private static final String motherpicture = "anc_picture";
    private static final String gps = "anc_gps";
    private static final String compHistory = "anc_comphistory";
    private static final String TB_ADVICE = "anc_advice";
    SQLiteDatabase dNew;
}
