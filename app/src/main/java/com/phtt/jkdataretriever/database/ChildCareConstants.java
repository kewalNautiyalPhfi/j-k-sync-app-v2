package com.phtt.jkdataretriever.database;

/**
 * Created by Sharma on 02-02-2016.
 */
public class ChildCareConstants {

    public final static ChildCareConstants INSTANCE = new ChildCareConstants();

    /**
     * singleton method
     * @return instance
     */
    public static synchronized ChildCareConstants getInstance(){

        return INSTANCE;
    }


    public final String  TABLE_CHILD_REGISTRATION                 = "childRegistration";
    public final String  TABLE_CHILD_PICTURE                      = "childPicture";
    public final String  TABLE_CHILD_GPS_LOCATOIN                 = "childGpsLocation";
    public final String  TABLE_CHILD_CLOSURE                      = "childClosure";
    public final String  TABLE_CHILD_BREASTFEEDING                = "childBreastfeeding";
    public final String  TABLE_CHILD_EXAM_MEASLES                 = "childExamMeasles";
    public final String  TABLE_CHILD_EXAM_DPT                     = "childExamDpt";
    public final String  TABLE_CHILD_ADVERSE_EVENT                = "childAdverseEvent";
    public final String  TABLE_CHILD_VAC_BCG                      = "childVacBCG";
    public final String  TABLE_CHILD_VAC_OPV_0                    = "childVacOPV_0";
    public final String  TABLE_CHILD_VAC_HEPA_B_0                 = "childVacHepatitisB_0";
    public final String  TABLE_CHILD_VAC_OPV_1                    = "childVacOPV_1";
    public final String  TABLE_CHILD_VAC_OPV_2                    = "childVacOPV_2";
    public final String  TABLE_CHILD_VAC_OPV_3                    = "childVacOPV_3";
    public final String  TABLE_CHILD_VAC_DPT_1                    = "childVacDPT_1";
    public final String  TABLE_CHILD_VAC_DPT_2                    = "childVacDPT_2";
    public final String  TABLE_CHILD_VAC_DPT_3                    = "childVacDPT_3";
    public final String  TABLE_CHILD_VAC_HEPA_B_1                 = "childVacHepatitisB_1";
    public final String  TABLE_CHILD_VAC_HEPA_B_2                 = "childVacHepatitisB_2";
    public final String  TABLE_CHILD_VAC_HEPA_B_3                 = "childVacHepatitisB_3";
    public final String  TABLE_CHILD_VAC_MEASLES                  = "childVacMeasles";
    public final String  TABLE_CHILD_VAC_VITAMIN_A                = "childVacVitamin_A";
    public final String  TABLE_CHILD_VAC_DPT_BOOSTER              = "childVacDPTBooster";
    public final String  TABLE_CHILD_VAC_POLIO_BOOSTER            = "childVacPolioBooster";
    public final String  TABLE_CHILD_REFER                        = "childRefer";
    public final String  TABLE_CHILD_FOLLOW_UP                    = "childFollowup";
    public final String  TABLE_CHILD_DETAILS                      = "childVisitDetail";
    public final String  TABLE_CHILD_DIARRHOEA                    = "childDiarrhoea";
    public final String  TABLE_CHILD_PHY_EXAM                     = "childPhyExam";
    public final String  TABLE_CHILD_FEVER                        = "childFever";
    public final String  TABLE_CHILD_MEASLES_SIGN                 = "childMeaslesSign";
    public final String  TABLE_CHILD_SUMMARY_NOTES                = "childSummaryNotes";
    public final String  TABLE_CHILD_ADVISE                       = "childAdvise";
    public final String  TABLE_CHILD_VAC_JE                       = "childVacJE";
    public final String  TABLE_CHILD_VAC_PENTAVALENT              = "childVacPentavalent";
    public final String  TABLE_CHILD_MALNUTRITION				  = "childMalnutrition";
}
