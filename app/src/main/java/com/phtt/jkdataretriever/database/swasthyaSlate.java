package com.phtt.jkdataretriever.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.phtt.jkdataretriever.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Sharma on 03-02-2016.
 */
public class swasthyaSlate  extends SQLiteOpenHelper {

    private static final int Database_Version = 1;// variable that defines the
    private static final String Database_Name = "swasthya_temp";// setting Regdatabase name
    SQLiteDatabase SsdDB;

    public swasthyaSlate(Context mContext, String tableName) {
        super(mContext, Database_Name, null, Database_Version);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        SsdDB = SQLiteDatabase.openDatabase(Constants.SWASTHYA_SLATE_APP_DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void copy() {

        FileChannel src = null;
        FileChannel dest = null;
        try {
            // set file object to the external path of the Database
            File sd = new File(Environment.getExternalStorageDirectory()
                    .toString() + "/swasthyaslate/");
            File data = Environment.getDataDirectory();// gets the android data directory
            sd.mkdirs();// make directory to the path specified to the object
            //db.openDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null, db.OPEN_READWRITE);
//            String currentDb = Constants.REG_APP_DATABASE_PATH;// location of current Database in the internal memory
            String backupDb = "swasthv2";//name of the Database in the external memory
            File curDb = new File(Constants.SWASTHYA_SLATE_APP_DATABASE_PATH);// file object that specifies the internal Database location

            File backup = new File(sd, backupDb);// File object that specifies the location of the external memory Database

            if (curDb.exists()) {

                src = new FileInputStream(curDb).getChannel();// Source channel of the Database
                dest = new FileOutputStream(backup).getChannel();// destination channel of the Database
                dest.transferFrom(src, 0, src.size());// coppies data from source to the destination


            }
        } catch (Exception e) {
            // TODO
            Log.e("Exception", "copy()", e);
        }
        finally{
            try{
                src.close();//close object
                dest.close();//close destination object
            }
            catch(Exception e){

            }
        }
    }



    public synchronized void addDataSwasthyaSlate(ContentValues values,  String TableName){
        SQLiteDatabase d = SQLiteDatabase.openDatabase(Constants.SWASTHYA_SLATE_APP_DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
//        ContentValues values = new ContentValues();
        ArrayList<String> list = new ArrayList<String>();
        if(values.containsKey("id")){
            values.remove("id");
        }


        if(values.containsKey("dateserver")){
            values.remove("dateserver");
        }
        if(values.containsKey("timeserver")){
            values.remove("timeserver");
        }


        String tableName ="";

        switch (TableName){
            case DbConstant.TABLE_WEIGHT:
                tableName= SwasthyaSlateConstants.getInstance().TABLE_WEIGHT;
                break;

            case DbConstant.TABLE_PULSE_OXIMETER:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_PULSE_OXIMETER;

                break;

            case DbConstant.TABLE_PREGNANCY_HCG:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_PREGNANCY_HCG;
                break;

            case DbConstant.TABLE_TEMPERATURE:
                if(values.containsKey("type")){
                    values.remove("type");
                }
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_TEMPERATURE;
                break;

            case DbConstant.TABLE_URINE_PROTEIN:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_URINE_PROTEIN;
                break;

            case DbConstant.TABLE_HIV:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_HIV;
                break;

            case DbConstant.TABLE_BLOOD_GLUCOSE:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_BLOOD_GLUCOSE;
                break;

            case DbConstant.TABLE_MALARIA:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_MALARIA;
                break;

            case DbConstant.TABLE_HEMOGLOBIN:
                if(values.containsKey("value")){
                    values.put("machine_result",values.getAsString("value"));
                    values.remove("value");
                }
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_HEMOGLOBIN;
                break;

            case DbConstant.TABLE_URINE_SUGAR:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_URINE_SUGAR;
                break;

            case DbConstant.TABLE_BLOOD_PRESSURE:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_BLOOD_PRESSURE;
                break;

            case DbConstant.TABLE_CHOLESTEROL_TEST:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_CHOLESTEROL_TEST;
                break;

            case DbConstant.TABLE_PT_INR_TEST:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_PT_INR_TEST;
                break;

            case DbConstant.TABLE_FOETAL_DOPPLER:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_FOETAL_DOPPLER;
                break;

            case DbConstant.TABLE_SYPHILLIS:
                tableName= SwasthyaSlateConstants.getInstance(). TABLE_SYPHILLIS;
                break;



            default:
                break;

        }
        if(!tableName.isEmpty()){


//            Cursor ti = d.rawQuery("PRAGMA table_info("+tableName+")", null);
//
//            if ( ti.moveToFirst() ) {
//                do {
//                    list.add(ti.getString(1));
//                    System.out.println("col: " + ti.getString(1));
//                } while (ti.moveToNext());
//            }

//            for(Iterator iteratora =  mJsonObject.keys(); iteratora.hasNext();) {
//                String key = (String) iteratora.next();
//
//
//
//                if (!key.equalsIgnoreCase("id")) {
//
//                    try {
//                        if (list.contains(key)) {
//                            String valuesAnc = mJsonObject.getString(key);
//                            values.put(key, valuesAnc);
//                        }
//
//
//                    } catch (JSONException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//                }
//
//            }
            if(values.containsKey("uploadkey")){
                        values.remove("uploadkey");
                    }
                values.put("uploadkey", 0);

//            values.put("editFlag", 1);
            long i=d.insert(tableName, null, values);
            this.copy();
        }
        d.close();

    }



}
