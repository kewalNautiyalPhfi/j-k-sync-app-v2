package com.phtt.jkdataretriever.database;

/**
 * Created by Sharma on 03-02-2016.
 */
public class SwasthyaSlateConstants {

    public final static SwasthyaSlateConstants INSTANCE = new SwasthyaSlateConstants();

    /**
     * singleton method
     * @return instance
     */
    public static synchronized SwasthyaSlateConstants getInstance(){

        return INSTANCE;
    }



    //Swasthya Slate Tables


    public final String TABLE_SWASTHYASLATE_BASE 				= "Swasthyslate_Base";
    public final String TABLE_WIDAL_ANTIGENS  					= "Widal_Antigens";
    public final String TABLE_WEIGHT  							= "Weight";
    public final String TABLE_UROBILINOGEN  					= "Urobilinogen";
    public final String TABLE_URINE_SUGAR  						= "Urine_Sugar";
    public final String TABLE_URINE_SPECIFIC_GRAVITY  			= "Urine_Specific_Gravity";
    public final String TABLE_URINE_PROTEIN  					= "Urine_Protein";
    public final String TABLE_URINE_PH  						= "Urine_PH";
    public final String TABLE_URINE_NITRITE						= "Urine_Nitrite";
    public final String TABLE_URINE_LEKOCYTES  					= "Urine_Lekocytes";
    public final String TABLE_URINE_KETONE  					= "Urine_Ketone";
    public final String TABLE_URINE_BLOOD  						= "Urine_Blood";
    public final String TABLE_URINE_BILIRUBIN  					= "Urine_Bilirubin";
    public final String TABLE_TROPONIN_TEST  					= "Troponin_Test";
    public final String TABLE_TEMPERATURE  						= "Temperature";
    public final String TABLE_SYPHILLIS  						= "Syphillis";
    public final String TABLE_SWASTHYASLATE_PATIENTS  			= "SwasthyaslatePatients";
    public final String TABLE_STETHOSCOPE  						= "Stethoscope";
    public final String TABLE_RH_FACTOR  						= "RH_Factor";
    public final String TABLE_PULSE_OXIMETER  					= "Pulse_Oximeter";
    public final String TABLE_PREGNANCY_HCG  					= "Pregnancy_HCG";
    public final String TABLE_ONSITE_TYPHOID  					= "Onsite_Typhoid";
    public final String TABLE_MALARIA  							= "Malaria";
    public final String TABLE_IMMUNODOT_HCV  					= "Immunodot_Test_Kit_HCV";
    public final String TABLE_HEPATITIS_B_VIRUS  				= "Hepatitis_B_Virus";
    public final String TABLE_HEIGHT  							= "Height";
    public final String TABLE_HEMOGLOBIN  						= "Haemoglobin";
    public final String TABLE_HIV 							    = "HIV";
    public final String TABLE_FOETAL_DOPPLER  					= "Foetal_Doppler";
    public final String TABLE_ECG  								= "ECG";
    public final String TABLE_C_REACTIVE  						= "C_Reactive_Pretein";
    public final String TABLE_BLOOD_PRESSURE  					= "Blood_Pressure";
    public final String TABLE_BLOOD_GROUPING  					= "Blood_Grouping_And_Typing";
    public final String TABLE_BLOOD_GLUCOSE  					= "Blood_Glucose";
    public final String TABLE_ANTI_STREPTOLYSIN  				= "Antistreptolysin";
    public final String TABLE_MACHINE_ERROR		  				= "machine_error";
    public final String TABLE_PATIENT_UPLOAD_HISTORY			= "patient_upload_history";
    public final String TABLE_PATIENT_TEST_ANALYSIS				= "patient_test_analysis";
    public final String TABLE_PT_INR_TEST 						= "pt_inr_test";
    public final String TABLE_CHOLESTEROL_TEST 					=  "cholesterol_test";



}

