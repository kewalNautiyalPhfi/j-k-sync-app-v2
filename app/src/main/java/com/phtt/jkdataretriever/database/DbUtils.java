package com.phtt.jkdataretriever.database;

import android.os.Environment;
import android.util.Log;

import com.phtt.jkdataretriever.utils.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class DbUtils {
	
	
	  public static final String TAG = "DbUtils";

	    /**
	     * singleton class
	     */
	    private static class SingletonHolder {
	        private static final DbUtils INSTANCE = new DbUtils();

	    }

	    public static synchronized DbUtils getInstance() {
	        return SingletonHolder.INSTANCE;
	    }

	    /*
	     * Method to copy the database form the internal memory to the external memory
	     */
	    @SuppressWarnings("resource")
		public void copy() {

	        try {
//						Log.v(TAG, "coping data to sd card..");
	            // set file object to the external path of the database
	            File sd = new File(Environment.getExternalStorageDirectory()
	                    .toString() + "/swasthyaslate/");
	            File data = Environment.getDataDirectory();
	            sd.mkdirs();

	            String currentDb = "/data/" + Constants.PACKAGE_NAME + "/databases/" + DbConstant.DATABASE_NAME;

	            String backupDb = DbConstant.DATABASE_NAME;
	            File curDb = new File(data, currentDb);

	            File backup = new File(sd, backupDb);

	            FileChannel src = new FileInputStream(curDb).getChannel();
	            FileChannel dest = new FileOutputStream(backup).getChannel();

	            if (curDb.exists()) {

	                dest.transferFrom(src, 0, src.size());
	                src.close();//close object
	                dest.close();//close destination object
							Log.v(TAG, "db copied successfully...");
	            } else {
	                try {
	                    curDb.createNewFile();
	                    dest.transferFrom(src, 0, src.size());
	                    src.close();
	                    dest.close();
			                    Log.v(TAG, "db copied successfully...new");
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }

	        } catch (Throwable e) {
//						Log.e(TAG, "copy(), Exception :"+e);
	            e.printStackTrace();
	        }


	    }
	

}
