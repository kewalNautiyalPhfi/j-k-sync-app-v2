package com.phtt.jkdataretriever.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	    private static String TAG = "DatabaseHelper";


	    public DatabaseHelper(Context mContext){
	        super(mContext,DbConstant.DATABASE_NAME, null,DbConstant.DATABASE_VERSION);
	    }


		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			String allCalls =  DbConstant.COLUMN_1 + " TEXT, "+ DbConstant.COLUMN_2 + " TEXT, " + DbConstant.COLUMN_3 + " TEXT, "
					+ DbConstant.COLUMN_4 + " TEXT, " + DbConstant.COLUMN_5 + " TEXT, " + DbConstant.COLUMN_6
					+ " TEXT," + DbConstant.COLUMN_7 + " TEXT," +DbConstant.COLUMN_8 + " TEXT," +DbConstant.COLUMN_9
					+ " TEXT," +  DbConstant.COLUMN_10 + " TEXT," +  DbConstant.COLUMN_11 + " TEXT," + DbConstant.COLUMN_12
					+ " TEXT," + DbConstant.COLUMN_13 + " TEXT," + DbConstant.COLUMN_14 + " TEXT," + DbConstant.COLUMN_15
					+ " TEXT," + DbConstant.COLUMN_16 + " TEXT," + DbConstant.COLUMN_17 + " TEXT," + DbConstant.COLUMN_18
					+ " TEXT," + DbConstant.COLUMN_19 + " TEXT," + DbConstant.COLUMN_20 + " TEXT," +DbConstant.COLUMN_21 + " TEXT,"
					+ DbConstant.COLUMN_22 + " TEXT," + DbConstant.COLUMN_23+ " TEXT," + DbConstant.COLUMN_24 + " TEXT,"
					+ DbConstant.COLUMN_25 + " TEXT";

			// TABLE LOGIN
			db.execSQL("create table " + DbConstant.TABLE_LOGIN + "(" + DbConstant.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ allCalls + ");");
			
		}


		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			
		}

}
