package com.phtt.jkdataretriever.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.phtt.jkdataretriever.utils.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/**
 * Created by Hemant on 2/1/2016.
 */
public class RegDataBase extends SQLiteOpenHelper {
    private static final int Database_Version = 1;// variable that defines the
    private static final String Database_Name = "reg_temp";// setting Regdatabase name
    SQLiteDatabase regDB;
    String tableName;
    private String geo = "geolocation";
    private String lang = "language";

    private String TABLE_LANG= "language";

    private String COLUMN_GID =  "pid";
    private String COLUMN_ID =  "id";
    private String COLUMN_LANG = "language";
    private String COLUMN_DATE = "datetime";
    private String COLUMN_KEY = "key";
    private String COLUMN_UPDATEKEY = "updatekey";





    public RegDataBase(Context mContext,String tab) {
        super(mContext, Database_Name, null, Database_Version);
        regDB = SQLiteDatabase.openDatabase(Constants.REG_APP_DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
        regDB.delete(tab, null, null);
        regDB.close();
        this.tableName=tab;

    }

    public void addRegData(ContentValues values) {

        try{

            regDB = SQLiteDatabase.openDatabase(Constants.REG_APP_DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
            ArrayList<String> list = new ArrayList<String>();

//            values.put("upflag", 1);
//            values.put("editFlag", 1);

//            regDB.delete(tableName, null, null);
            long i = 0;
            if(tableName!=null && values !=null) {
                i = regDB.insert(tableName, null, values);
                if(tableName.equalsIgnoreCase(geo)){
                    ContentValues Newvalue = null;
                   if(values.get("state").equals("Jammu and Kashmir")) {

                       Newvalue=new ContentValues();
                       Newvalue.put(COLUMN_GID, values.getAsString("pid"));
                       Newvalue.put(COLUMN_ID, values.getAsString("id"));
                       Newvalue.put(COLUMN_LANG, "English");
                       Newvalue.put(COLUMN_DATE, 0);
                       Newvalue.put(COLUMN_KEY, 0);
                       Newvalue.put(COLUMN_UPDATEKEY,0);
                        regDB.insert(TABLE_LANG,null,Newvalue);

                    }else if(values.get("state").equals("जम्मू कश्मीर")){
                       Newvalue=new ContentValues();
                       Newvalue.put(COLUMN_GID,values.getAsString("pid"));
                       Newvalue.put(COLUMN_ID,values.getAsString("id"));
                       Newvalue.put(COLUMN_LANG, "Hindi");
                       Newvalue.put(COLUMN_DATE, 0);
                       Newvalue.put(COLUMN_KEY, 0);
                       Newvalue.put(COLUMN_UPDATEKEY,0);
                       regDB.insert(TABLE_LANG,null,Newvalue);


                   }
                }
            }

            Log.d("Data_inserted "+i,""+String.valueOf(values));



//            FileChannel src = null;
//
//            FileChannel dest = null;
//            try {
//                // set file object to the external path of the Database
//                File sd = new File(Environment.getExternalStorageDirectory()
//                        .toString() + "/swasthyaslate/");
//                File data = Environment.getDataDirectory();// gets the android data directory
//                sd.mkdirs();// make directory to the path specified to the object
//                //db.openDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null, db.OPEN_READWRITE);
//                String currentDb = Constants.REG_APP_DATABASE_PATH;// location of current Database in the internal memory
//                String backupDb = "Registration";//name of the Database in the external memory
//                File curDb = new File(data, currentDb);// file object that specifies the internal Database location
//
//                File backup = new File(sd, backupDb);// File object that specifies the location of the external memory Database
//
//                if (curDb.exists()) {
//
//                    src = new FileInputStream(curDb).getChannel();// Source channel of the Database
//                    dest = new FileOutputStream(backup).getChannel();// destination channel of the Database
//                    dest.transferFrom(src, 0, src.size());// copies data from source to the destination
//
//
//                }
            } catch (Exception e) {
                // TODO
                Log.e("Exception", "copy()", e);
            }
            finally{
                try{
//                    src.close();//close object
//                    dest.close();//close destination object
                }
                catch(Exception e){

                }
            }
            copy();
            regDB.close();
            //this.copy(incDB);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//        regDB = SQLiteDatabase.openDatabase(Constants.ANC_APP_DATABASE_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void copy() {

        FileChannel src = null;
        FileChannel dest = null;
        try {
            // set file object to the external path of the Database
            File sd = new File(Environment.getExternalStorageDirectory()
                    .toString() + "/swasthyaslate/");
            File data = Environment.getDataDirectory();// gets the android data directory
            sd.mkdirs();// make directory to the path specified to the object

            String currentDb = "/data/phfi.aht.registration/databases/Registration";// location of current Database in the internal memory
            String backupDb = "Registration";//name of the Database in the external memory
            File curDb = new File(data, currentDb);// file object that specifi
            // es the internal Database location

            File backup = new File(sd, backupDb);// File object that specifies the location of the external memory Database

            if (curDb.exists()) {

                src = new FileInputStream(curDb).getChannel();// Source channel of the Database
                dest = new FileOutputStream(backup).getChannel();// destination channel of the Database
                dest.transferFrom(src, 0, src.size());// coppies data from source to the destination


            }
        } catch (Exception e) {
            // TODO
            Log.e("Exception", "copy()", e);
        }
        finally{
            try{
                src.close();//close object
                dest.close();//close destination object
            }
            catch(Exception e){

            }
        }
    }
}
