package com.phtt.jkdataretriever.database;

import android.net.Uri;

/**
 * Created by HP on 1/21/2016.
 */
public class DbConstant {

    // DATABASE NAME DECLARATION
    public static final String DATABASE_NAME = "jkDataRetriever";

    // DATABASE VERSION DECLARATION
    public static final int DATABASE_VERSION = 001;
    //TABLE ID DECLARATIOM

    public static final int ID_TABLE_LOGIN     			 = 1;
    //DATABASE AUTHORITY
    public static final String AUTHORITY = "com.phtt.jkdataretrieverDatabase";


    //TABLE DECLARATION

    public static final String TABLE_LOGIN = "login";

    //CONTENT URI DECLARATION
    public static final Uri URI_TABLE_LOGIN = Uri.parse("content://" + AUTHORITY + "/" + TABLE_LOGIN);

    //COLUMN DECLARATION

    public final static String COLUMN_ID 		           = "id";


    //ALL TABLE ID
    public static final int[] ALL_TABLE_ID ={

            ID_TABLE_LOGIN
    };

    //COLUMN DECLARATION

    public final static String COLUMN_1 = "col1";
    public final static String COLUMN_2 = "col2";
    public final static String COLUMN_3 = "col3";
    public final static String COLUMN_4 = "col4";
    public final static String COLUMN_5 = "col5";
    public final static String COLUMN_6 = "col6";
    public final static String COLUMN_7 = "col7";
    public final static String COLUMN_8 = "col8";
    public final static String COLUMN_9 = "col9";
    public final static String COLUMN_10 = "col10";
    public final static String COLUMN_11 = "col11";
    public final static String COLUMN_12 = "col12";
    public final static String COLUMN_13 = "col13";
    public final static String COLUMN_14 = "col14";
    public final static String COLUMN_15 = "col15";
    public final static String COLUMN_16 = "col16";
    public final static String COLUMN_17 = "col17";
    public final static String COLUMN_18 = "col18";
    public final static String COLUMN_19 = "col19";
    public final static String COLUMN_20 = "col20";
    public final static String COLUMN_21 = "col21";
    public final static String COLUMN_22 = "col22";
    public final static String COLUMN_23 = "col23";
    public final static String COLUMN_24 = "col24";
    public final static String COLUMN_25 = "col25";
    //All Common coloum

    public static final String[] columnList = {

            COLUMN_1,
            COLUMN_2,
            COLUMN_3,
            COLUMN_4,
            COLUMN_5,
            COLUMN_6,
            COLUMN_7,
            COLUMN_8,
            COLUMN_9,
            COLUMN_10,
            COLUMN_11,
            COLUMN_12,
            COLUMN_13,
            COLUMN_14,
            COLUMN_15,
            COLUMN_16,
            COLUMN_17,
            COLUMN_18,
            COLUMN_19,
            COLUMN_20,
            COLUMN_21,
            COLUMN_22,
            COLUMN_23,
            COLUMN_24,
            COLUMN_25
    };


    //URI OF ALL TABLES
    public static final Uri[] ALL_URI = {
            URI_TABLE_LOGIN
    };


    //ALL TABLE NAMES
    public static final String[] ALL_TABLES = {
            TABLE_LOGIN
    };


    // PNC db table name
    public static final String TABLE_MOTHER_POSTPARTUM_CARE_N_HYGEINE 			= 	"motherpostpartumcarenhygeine";
    public static final String TABLE_MOTHER_NUTRITION 							=	"mothernutrition";
    public static final String TABLE_MOTHER_CONTRACEPTION 						= 	"mothercontraception";
    public static final String TABLE_MOTHER_BREASTFEEDING 						= 	"motherbreastfeeding";
    public static final String TABLE_REGISTRATIONOF_BIRTH 						= 	"registrationofbirth";
    public static final String TABLE_MOTHER_IFA_TABLETS  						= 	"motherifatablets";
    public static final String TABLE_MOTHER_DANGER_SIGN 						= 	"motherdangersign";
    public static final String TABLE_MOTHER_COMPLICATION						= 	"mothercomplication";
    public static final String TABLE_MOTHER_PHYSICAL_EXAMINATION 				= 	"motherphysicalexamination";
    public static final String TABLE_MOTHER_COUNSELLING_ON_BREASTFEEDING 		= 	"mothercounsellingonbreastfeeding";
    public static final String TABLE_MOTHER_COUNSELLING_REGARDING_INFANT_CARE	= 	"mothercounsellingregardinginfantcare";
    public static final String TABLE_INFANT_DANDER_SIGN 						= 	"infantdandersign";
    public static final String TABLE_INFANT_COMPLICATIONS 						= 	"infantcomplications";
    public static final String TABLE_INFANT_COUNSELLING_ON_WEIGHTLOSS 			= 	"infantcounsellingonweightloss";
    public static final String TABLE_INFANT_COUNSELLING_ON_BABY_HYGEINE 		= 	"infantcounsellingonbabyhygeine";
    public static final String TABLE_INFANT_PHYSICAL_EXAMINATION 				= 	"infantphysicalexamination";
    public static final String TABLE_REFERRALOF_MOTHERAND_INFANT 				= 	"referralofmotherandinfant";
    public static final String TABLE_INFANT_DEATH 								= 	"infantdeath";
    public static final String TABLE_MOTHER_DEATH 								= 	"motherdeath";

    // pnc common table names
    public static final String TABLE_PNC_REGISTRATION 							= 	"pncregistration";
    public static final String TABLE_PNC_PICTURE 								= 	"pncpicture";
    public static final String TABLE_PNC_GPS_LOCATION 							= 	"pncgpslocation";
    public static final String TABLE_PNC_HISTORY								= 	"pnchistory";
    public static final String TABLE_PNC_REFERRAL 								= 	"pncreferral";
    public static final String TABLE_PNC_FOLLOWUP 								= 	"pncfollowup";
    public static final String TABLE_PNC_VISIT_DETAILS							=   "pncvisitdetails";

    public static final String TABLE_PNC_SUMARRY_NOTES							=   "pncsummarynotes";
    public static final String TABLE_PNC_ADVICE							    	=   "pncadvice";

    public static final String TABLE_PNC_REFERRAL_CHILD 						= 	"pncchildreferral";
    public static final String TABLE_PNC_FOLLOWUP_CHILD 						= 	"pncchildfollowup";
    public static final String TABLE_PNC_ADVICE_CHILD							=   "pncchildadvice";
    public static final String TABLE_PNC_SUMARRY_NOTES_CHILD					=   "pncChildSummaryNotes";



    // CHILD_CARE_TABLES

    public static final String TABLE_CHILD_REGISTRATION                 = "childregistration";
    public static final String TABLE_CHILD_PICTURE                      = "childpicture";
    public static final String TABLE_CHILD_GPS_LOCATOIN                 = "childgpslocation";
    public static final String TABLE_CHILD_CLOSURE                      = "childclosure";
    public static final String TABLE_CHILD_BREASTFEEDING                = "childbreastfeeding";
    public static final String TABLE_CHILD_EXAM_MEASLES                 = "childexammeasles";
    public static final String TABLE_CHILD_EXAM_DPT                     = "childexamdpt";
    public static final String TABLE_CHILD_ADVERSE_EVENT                = "childadverseevent";
    public static final String TABLE_CHILD_VAC_BCG                      = "childvacbcg";
    public static final String TABLE_CHILD_VAC_OPV_0                    = "childvacopv_0";
    public static final String TABLE_CHILD_VAC_HEPA_B_0                 = "childvachepatitisb_0";
    public static final String TABLE_CHILD_VAC_OPV_1                    = "childvacopv_1";
    public static final String TABLE_CHILD_VAC_OPV_2                    = "childvacopv_2";
    public static final String TABLE_CHILD_VAC_OPV_3                    = "childvacopv_3";
    public static final String TABLE_CHILD_VAC_DPT_1                    = "childvacdpt_1";
    public static final String TABLE_CHILD_VAC_DPT_2                    = "childvacdpt_2";
    public static final String TABLE_CHILD_VAC_DPT_3                    = "childvacdpt_3";
    public static final String TABLE_CHILD_VAC_HEPA_B_1                 = "childvachepatitisb_1";
    public static final String TABLE_CHILD_VAC_HEPA_B_2                 = "childvachepatitisb_2";
    public static final String TABLE_CHILD_VAC_HEPA_B_3                 = "childvachepatitisb_3";
    public static final String TABLE_CHILD_VAC_MEASLES                  = "childvacmeasles";
    public static final String TABLE_CHILD_VAC_VITAMIN_A                = "childvacvitamin_a";
    public static final String TABLE_CHILD_VAC_DPT_BOOSTER              = "childvacdptbooster";
    public static final String TABLE_CHILD_VAC_POLIO_BOOSTER            = "childvacpoliobooster";
    public static final String TABLE_CHILD_REFER                        = "childrefer";
    public static final String TABLE_CHILD_FOLLOW_UP                    = "childfollowup";
    public static final String TABLE_CHILD_DETAILS                      = "childvisitdetail";
    public static final String TABLE_CHILD_DIARRHOEA                    = "childdiarrhoea";
    public static final String TABLE_CHILD_PHY_EXAM                     = "childphyexam";
    public static final String TABLE_CHILD_FEVER                        = "childfever";
    public static final String TABLE_CHILD_MEASLES_SIGN                 = "childmeaslessign";
    public static final String TABLE_CHILD_SUMMARY_NOTES                = "childsummarynotes";
    public static final String TABLE_CHILD_ADVISE                       = "childadvise";
    public static final String TABLE_CHILD_VAC_JE                       = "childvacje";
    public static final String TABLE_CHILD_VAC_PENTAVALENT              = "childvacpentavalent";
    public static final String TABLE_CHILD_MALNUTRITION				  = "childmalnutrition";


    //Swasthya Slate Tables

    public static final String TABLE_WEIGHT  							= "weight";
    public static final String TABLE_PULSE_OXIMETER  					= "pulse_oximeter";
    public static final String TABLE_PREGNANCY_HCG  					= "pregnancy_hcg";
    public static final String TABLE_TEMPERATURE  						= "temperature";
    public static final String TABLE_URINE_PROTEIN  					= "urine_protein";
    public static final String TABLE_HIV 							    = "hiv";
    public static final String TABLE_BLOOD_GLUCOSE  					= "blood_glucose";
    public static final String TABLE_MALARIA  							= "malaria";
    public static final String TABLE_HEMOGLOBIN  						= "haemoglobin";
    public static final String TABLE_URINE_SUGAR  						= "urine_sugar";
    public static final String TABLE_BLOOD_PRESSURE  					= "blood_pressure";
    public static final String TABLE_CHOLESTEROL_TEST 					= "cholesterol_test";
    public static final String TABLE_PT_INR_TEST 						= "pt_inr_test";
    public static final String TABLE_FOETAL_DOPPLER  					= "foetal_doppler";
    public static final String TABLE_SYPHILLIS  						= "syphillis";































}
